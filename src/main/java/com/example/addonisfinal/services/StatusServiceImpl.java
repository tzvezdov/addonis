package com.example.addonisfinal.services;

import com.example.addonisfinal.exceptions.EntityNotFoundException;
import com.example.addonisfinal.models.Status;
import com.example.addonisfinal.repositories.StatusRepository;
import com.example.addonisfinal.services.contracts.StatusService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class StatusServiceImpl implements StatusService {

    private final StatusRepository statusRepository;

    @Override
    public Status getStatusByName(String name) {
        if (!statusRepository.existsByName(name)) {
            throw new EntityNotFoundException(
                    "Status", "name", name);
        }

        return statusRepository.findByName(name);
    }

    @Override
    public void createStatus(Status status) {
        statusRepository.saveAndFlush(status);
    }
}
