package com.example.addonisfinal.services;

import com.example.addonisfinal.exceptions.EntityNotFoundException;
import com.example.addonisfinal.repositories.RatingRepository;
import com.example.addonisfinal.services.contracts.RatingService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
@RequiredArgsConstructor

public class RatingServiceImpl implements RatingService {

    private final RatingRepository ratingRepository;

    @Override
    public Double getAvgRatingOfAddon(int addonId) {
        Optional<Double> rating = ratingRepository
                .findAverageRatingOfAddon(addonId);

        return rating.orElseThrow(() -> new EntityNotFoundException("No such rating found."));
    }
}
