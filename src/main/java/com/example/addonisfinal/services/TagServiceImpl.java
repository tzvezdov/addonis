package com.example.addonisfinal.services;

import com.example.addonisfinal.exceptions.EntityNotFoundException;
import com.example.addonisfinal.models.Tag;
import com.example.addonisfinal.repositories.TagRepository;
import com.example.addonisfinal.services.contracts.TagService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    @Override
    public List<Tag> getAll() {
        return tagRepository.findAll();
    }

    @Override
    public Tag getByName(String name) {

        Optional<Tag> tag = tagRepository.findByName(name);

        return tag.orElseThrow(() -> new EntityNotFoundException("Tag", "name", name));
    }

    @Override
    public Tag addTag(Tag tag) {
        return tagRepository.saveAndFlush(tag);
    }

    @Override
    public void deleteTag(Tag tag) {
        tagRepository.delete(tag);
    }
}
