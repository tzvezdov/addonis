package com.example.addonisfinal.services;

import com.example.addonisfinal.models.ConfirmationToken;
import com.example.addonisfinal.repositories.ConfirmationTokenRepository;
import com.example.addonisfinal.services.contracts.ConfirmationTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ConfirmationTokenServiceImpl implements ConfirmationTokenService {

    private final ConfirmationTokenRepository tokenRepository;

    public void saveConfirmationToken(ConfirmationToken confirmationToken) {

        tokenRepository.save(confirmationToken);
    }

    @Override
    public Optional<ConfirmationToken> getToken(String token) {
        return tokenRepository.findByToken(token);
    }

    @Override
    public void setConfirmedAt(String token) {

        Optional<ConfirmationToken> confirmationToken = tokenRepository.findByToken(token);
        confirmationToken.ifPresent(value -> value.setConfirmedAt(LocalDateTime.now()));
        tokenRepository.saveAndFlush(confirmationToken.get());
    }

}
