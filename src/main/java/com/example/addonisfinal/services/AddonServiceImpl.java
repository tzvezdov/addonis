package com.example.addonisfinal.services;

import com.example.addonisfinal.exceptions.DuplicateEntityException;
import com.example.addonisfinal.exceptions.EntityNotFoundException;
import com.example.addonisfinal.exceptions.UnauthorizedOperationException;
import com.example.addonisfinal.helpers.TagHelper;
import com.example.addonisfinal.models.*;
import com.example.addonisfinal.models.dtos.addon.AddonDto;
import com.example.addonisfinal.models.dtos.addon.CodeInputDto;
import com.example.addonisfinal.repositories.AddonRepository;
import com.example.addonisfinal.repositories.ConfirmationCodeRepository;
import com.example.addonisfinal.repositories.JointAddonRepository;
import com.example.addonisfinal.repositories.RatingRepository;
import com.example.addonisfinal.services.contracts.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import static com.example.addonisfinal.services.GitHubDataServiceImpl.GITHUB_PREFIX;

@Service
@RequiredArgsConstructor
public class AddonServiceImpl implements AddonService {

    public static final String ON_HOLD = "on hold";
    private final AddonRepository addonRepository;
    private final RatingRepository ratingRepository;
    private final UserService userService;
    private final JointAddonRepository jointAddonRepository;
    private final IDEService ideService;
    private final StatusService statusService;
    private final TagHelper tagHelper;
    private final GitHubDataService gitHubDataService;

    private final ConfirmationCodeService confirmationCodeService;

    private final EmailSender emailSender;

    @Override
    public List<Addon> getAll() {
        return addonRepository.findAll();
    }

    @Override
    public Addon getById(int id) {
        Optional<Addon> addon = addonRepository.findAddonById(id);

        return addon.orElseThrow(() -> new EntityNotFoundException("Addon", id));
    }

    @Override
    @Transactional
    public void saveAddon(Addon addon, User creator, AddonDto addonDto) throws IOException {
        if (addonRepository.existsByNameAndIdIsNot(addon.getName(),0)) {
            throw new DuplicateEntityException("Addon", "name", addon.getName());
        }

        if (addonRepository.existsByOriginLink(addonDto.getOriginLink())) {
            throw new DuplicateEntityException("Addon", "origin link", addonDto.getOriginLink());
        }

        if (!creator.isEnabled() || creator.isDeleted() || creator.getUserDetails().isBlocked()) {
            throw new UnauthorizedOperationException("You are not allowed this action.");
        }

        if (addonDto.getFile().isEmpty()) {
            throw new DuplicateEntityException("You need to attach a file");
        }

        IDE ide = ideService.getByName(addonDto.getIdeName());

        addon.setIde(ide);

        Status status = statusService.getStatusByName("unconfirmed");

        addon.setStatus(status);
        addon.setCreator(creator);
        addon.setCreatedOn(LocalDateTime.now());
        addon.setGitHubData(getGitHubData(addonDto.getOriginLink(), new GitHubData()));
        if (!addonDto.getImage().isEmpty()) {
            addon.setAvatar(Base64.getEncoder().encodeToString(addonDto.getImage().getBytes()));
        } else {
            addon.setAvatar(null);
        }
        addon.setOriginLink(addonDto.getOriginLink());
        addon.setConfirmed(false);

        Random rnd = new Random();
        int number = rnd.nextInt(90000) + 100000;
        String code = String.valueOf(number);
        ConfirmationCode confirmationCode = new ConfirmationCode(code,addon,addon.getCreator());

        emailSender.sendConfirmationCode(addon.getCreator().getEmail(), buildConfirmationCodeEmail(addon.getCreator().getUsername(), addon.getName(), confirmationCode.getCode()));

        addonRepository.saveAndFlush(addon);
        confirmationCodeService.saveConfirmationCode(confirmationCode);
    }

    @Override
    public Addon findByName(String name) {
        Optional<Addon> addon = addonRepository.findAddonByName(name);

        return addon.orElseThrow(() -> new EntityNotFoundException("Addon", "name", name));
    }

    @Override
    public void confirmAddon(Addon addon,CodeInputDto codeInputDto) {

        Optional<ConfirmationCode> cfCode = confirmationCodeService.getCodeByAddonId(addon.getId());

        ConfirmationCode confirmCode = cfCode.orElseThrow(() ->
                new EntityNotFoundException("Confirmation code","code",codeInputDto.getCode()));


        if (confirmCode.getCode().equals(codeInputDto.getCode())) {
            addon.setConfirmed(true);
            Status status = statusService.getStatusByName("PENDING");
            addon.setStatus(status);
            addonRepository.save(addon);
        } else {
            throw new IllegalArgumentException("The code that you provided was wrong.");
        }
    }

    @Override
    public List<Addon> findUnverifiedAddonsForUser(int id) {
        return addonRepository.findUnverifiedAddonsForUser(id);
    }


    @Override
    public void updateAddon(Addon addon, AddonDto addonDto, User userToBeChecked) throws IOException {

        if (!addon.getCreator().equals(userToBeChecked)) {
            throw new UnauthorizedOperationException("Only the owner of the addon can modify it");
        }

        if (addonRepository.existsByNameAndIdIsNot(addonDto.getName(), addon.getId())) {
            throw new DuplicateEntityException("Addon", "name", addonDto.getName());
        }
        addon.setName(addonDto.getName());
        addon.setDescription(addonDto.getDescription());
        addon.setOriginLink(addonDto.getOriginLink());
        addon.setTags(tagHelper.prepareTags(addonDto.getTags()));
        addon.setLastModifiedOn(LocalDateTime.now());
        addon.setGitHubData(getGitHubData(addonDto.getOriginLink(), addon.getGitHubData()));
        addon.setIde(ideService.getByName(addonDto.getIdeName()));
        if (!addonDto.getImage().isEmpty()) {
            addon.setAvatar(Base64.getEncoder().encodeToString(addonDto.getImage().getBytes()));
        }
        addonRepository.saveAndFlush(addon);
    }

    @Override
    public void simpleUpdate(Addon addon) {
        addonRepository.saveAndFlush(addon);
    }

    @Override
    public void uploadFile(Addon addon) {
        addonRepository.saveAndFlush(addon);
    }

    @Override
    public void promoteToMaintainer(String addonName, String userToPromote, User user) {

        User userMaintainer = userService.getUsername(userToPromote);
        JointAddon jointAddon = new JointAddon();
        Optional<Addon> addon = addonRepository.findAddonByName(addonName);
        addon.orElseThrow(() -> new EntityNotFoundException("Addon", "name", addonName));

        if (addon.get().getCreator().equals(user)) {
            jointAddon.setAddon(addon.get());
            jointAddon.setUser(userMaintainer);
            StringBuilder sb = new StringBuilder();
            sb.append(addon.get().getMaintainers()).append(userToPromote).append(",");
            addon.get().setMaintainers(sb.toString());
            addonRepository.save(addon.get());
            jointAddonRepository.save(jointAddon);
        } else {
            throw new UnauthorizedOperationException("You are not the creator of this addon!");
        }
    }

    @Override
    public void revokeMaintainerRights(String addonName, String userToDemote, User user) {


        Optional<Addon> addon = addonRepository.findAddonByName(addonName);
        addon.orElseThrow(() -> new EntityNotFoundException("Addon", "name", addonName));

        if (!addon.get().getCreator().equals(user)) {
            throw new UnauthorizedOperationException("You are not the owner of the addon.");
        }


        User userDemoted = userService.getUsername(userToDemote);

        JointAddon jointAddon = jointAddonRepository.findJointAddonBy(addon.orElseThrow(()
                -> new EntityNotFoundException("Addon", "name", addonName)).getId(), userDemoted.getId());

        jointAddonRepository.delete(jointAddon);
        addon.get().setMaintainers(addon.get().getMaintainers().replace(userToDemote + ",", ""));
        addonRepository.save(addon.get());

    }

    @Override
    public void approveAddon(User userToBeChecked, int addonId) {

        Optional<Addon> addonForApproval = addonRepository.findAddonById(addonId);
        addonForApproval.orElseThrow(() -> new EntityNotFoundException("Addon", "id", String.valueOf(addonId)));
        Status approvedStatus = statusService.getStatusByName("ACTIVE");

        if (addonForApproval.get().getStatus().equals(approvedStatus)) {
            throw new DuplicateEntityException("Addon", "Status", addonForApproval.get().getStatus().toString());
        }

        if (userToBeChecked.isAdmin()) {
            addonForApproval.get().setStatus(approvedStatus);
            addonRepository.save(addonForApproval.get());
        } else {
            throw new UnauthorizedOperationException("Only admins can approve addons!");
        }
    }

    @Override
    public void delete(User user, int id) {
        Optional<Addon> addon = addonRepository.findAddonById(id);
        addon.orElseThrow(() -> new EntityNotFoundException("Addon", id));

        if (!addon.get().getCreator().equals(user) && !user.isAdmin()) {
            throw new UnauthorizedOperationException("Only the creator of the post or a moderator can delete it.");
        }

        addon.get().setDeleted(true);
        addonRepository.saveAndFlush(addon.get());
    }


    public GitHubData getGitHubData(String url, GitHubData gitHubData) throws IOException {

        if (!gitHubDataService.validateLink(url)) {
            throw new IllegalArgumentException("The origin link that you entered is invalid.");
        }

        url = url.substring(GITHUB_PREFIX.length());
        gitHubData.setPullCount(gitHubDataService.getPullRequestCount(url));
        gitHubData.setOpenIssuesCount(gitHubDataService.getOpenIssueCount(url));
        gitHubData.setLastCommitDate(gitHubDataService.getLastCommitDate(url));
        gitHubData.setLastCommitTitle(gitHubDataService.getLastCommitTitle(url));
        gitHubData.setEnabled(true);

        return gitHubData;
    }

    public void rateAddon(int addonId, User user, Rating rating2) {
        Addon addon = getById(addonId);

        boolean hasRating = ratingRepository.existsByUserAndAddon_Id(user, addonId);

        Rating rating;
        if (hasRating) {
            rating = ratingRepository.findRatingByAddon_IdAndUser(addonId, user);
            rating.setRating(rating2.getRating());
            addon.getRating().add(rating);
        } else {
            rating = new Rating();
            rating.setRating(rating2.getRating());
            rating.setUser(user);
            rating.setAddon(addon);
            ratingRepository.saveAndFlush(rating);
            addon.getRating().add(rating);
        }

        addonRepository.saveAndFlush(addon);
    }

    @Override
    public Page<Addon> getAllByMultipleFilters(String addonName,
                                               String creatorUsername, String tags,
                                               String ideName, String orderBy,
                                               int pageNumber, int maxResultsPerPage) {

        Pageable pageable = PageRequest.of(pageNumber - 1, maxResultsPerPage);

        if (orderBy.equals("upload_date")) {
            return addonRepository.findDistinctByStatus_NameAndNameLikeAndCreator_UsernameLikeAndTags_NameLikeAndIde_NameLikeAndDeletedIsFalseOrderByCreatedOnDesc(
                    "active", addonName, creatorUsername, tags, ideName, pageable);
        } else if (orderBy.equals("last_commit_date")) {
            return addonRepository.findDistinctByStatus_NameAndNameLikeAndCreator_UsernameLikeAndTags_NameLikeAndIde_NameLikeAndDeletedIsFalseOrderByGitHubData_LastCommitDateDesc(
                    "active", addonName, creatorUsername, tags, ideName, pageable);
        } else if (orderBy.equals("downloads_count")) {
            return addonRepository.findDistinctByStatus_NameAndNameLikeAndCreator_UsernameLikeAndTags_NameLikeAndIde_NameLikeAndDeletedIsFalseOrderByDownloadsCountDesc(
                    "active", addonName, creatorUsername, tags, ideName, pageable);
        } else if (orderBy.equals("rating")) {
            return addonRepository.findDistinctByStatus_NameAndNameLikeAndCreator_UsernameLikeAndTags_NameLikeAndIde_NameLikeAndDeletedIsFalseOrderByRatingRatingDesc(
                    "active", addonName, creatorUsername, tags, ideName, pageable);
        }
        return addonRepository.findDistinctByStatus_NameAndNameLikeAndCreator_UsernameLikeAndTags_NameLikeAndIde_NameLikeAndDeletedIsFalseOrderByName(
                "active", addonName, creatorUsername, tags, ideName, pageable);


    }

    @Override
    public List<Addon> getByUser(User user) {
        return addonRepository.findByCreatorAndDeletedIsFalse(user);
    }


    @Override
    public void notifyUser(User user, Addon addon) {

        addon.setStatus(statusService.getStatusByName(ON_HOLD));

        addonRepository.saveAndFlush(addon);

        emailSender.send(user.getEmail(), buildNotifyUserEmail(user.getUsername(), addon.getName()));
    }

    private String buildNotifyUserEmail(String name, String addonName) {
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
                "\n" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                "\n" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                "        \n" +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                "          <tbody><tr>\n" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td style=\"padding-left:10px\">\n" +
                "                  \n" +
                "                    </td>\n" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Did you know we are the best addons registry?</span>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "              </a>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                "      <td>\n" +
                "        \n" +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "\n" +
                "\n" +
                "\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> It seems that your recently uploaded addon: " + addonName + " was put on HOLD by one of our admins. Maybe there was inappropriate wording or content there.</p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"></p></blockquote>\n <p>For more information please contact our customer support!</p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                "\n" +
                "</div></div>";
    }


    private String buildConfirmationCodeEmail(String name, String addonName, String confirmationCode) {
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
                "\n" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                "\n" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                "        \n" +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                "          <tbody><tr>\n" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td style=\"padding-left:10px\">\n" +
                "                  \n" +
                "                    </td>\n" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Did you know we are the best addons registry?</span>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "              </a>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                "      <td>\n" +
                "        \n" +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "\n" +
                "\n" +
                "\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> Here is the unique 6-digit verification code for your addon: " + addonName + " </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"></p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> " + confirmationCode + " </p></blockquote>\n <p><br> For more information or problems with the code please contact our customer support!</p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                "\n" +
                "</div></div>";
    }
}
