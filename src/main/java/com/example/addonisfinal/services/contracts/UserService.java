package com.example.addonisfinal.services.contracts;

import com.example.addonisfinal.models.User;
import com.example.addonisfinal.models.dtos.ContactUsDto;
import com.example.addonisfinal.models.dtos.user.InviteFriendDto;
import com.example.addonisfinal.models.dtos.user.UserEditPasswordDto;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface UserService {

    List<User> getAllUsers();

    List<User> getAllPendingVerificationUsers();

    User getUsername(String username);

    User getById(int id);

    void create(User user);

    void delete(int id, int userToBeCheckedId);

    void changePermissions(int id, User admin);

    void block(int userId, User admin);

    void unblock(int userId, User admin);

    List<User> getMaintainers(Integer addonId);

    List<User> supplyUsers();

    void update(int id, User user, User userToBeChecked);

    void changePassword(int id, UserEditPasswordDto dto, User userToBeChecked);

    void uploadAvatar(int id, User userToBeChecked, MultipartFile multipartFile) throws IOException;

    void confirmToken(String token);

    void uploadVerificationPictures(int id, User userToBeChecked, MultipartFile selfie, MultipartFile idCard) throws IOException;

    void verify(int userId, User admin);

    void inviteFriend(User user, String email);

    void sendContactUsEmail(ContactUsDto contactUsDto);

    Page<User> getAllByMultipleFilters(String username, int pageNumber, int maxResultsPerPage);

    void resendToken(int id, InviteFriendDto user);
}
