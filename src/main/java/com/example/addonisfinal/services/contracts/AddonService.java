package com.example.addonisfinal.services.contracts;

import com.example.addonisfinal.models.Addon;
import com.example.addonisfinal.models.Rating;
import com.example.addonisfinal.models.User;
import com.example.addonisfinal.models.dtos.addon.AddonDto;
import com.example.addonisfinal.models.dtos.addon.CodeInputDto;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

public interface AddonService {

    List<Addon> getAll();

    Addon getById(int id);

    void saveAddon(Addon addon, User creator, AddonDto addonDto) throws IOException;

    Addon findByName(String name);

    void updateAddon(Addon addon, AddonDto addonDto, User userToBeChecked) throws IOException;

    void simpleUpdate(Addon addon);

    void uploadFile(Addon addon);

    void promoteToMaintainer(String addonName, String userToPromote, User user);

    void revokeMaintainerRights(String addonName, String userToDemote, User user);

    void approveAddon(User userToBeChecked, int addonId);

    void notifyUser(User user, Addon addon);

    void delete(User user, int id);

    void rateAddon(int id, User user, Rating rating);

    Page<Addon> getAllByMultipleFilters(String addonName, String creatorUsername, String tags, String ideName, String orderBy, int pageNumber, int maxResultsPerPage);

    List<Addon> getByUser(User user);

    void confirmAddon(Addon addon, CodeInputDto codeInputDto);

    List<Addon> findUnverifiedAddonsForUser(int id);
}

