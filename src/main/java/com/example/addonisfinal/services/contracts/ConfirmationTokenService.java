package com.example.addonisfinal.services.contracts;


import com.example.addonisfinal.models.ConfirmationToken;

import java.util.Optional;

public interface ConfirmationTokenService {

    void saveConfirmationToken(ConfirmationToken confirmationToken);

    Optional<ConfirmationToken> getToken(String token);

    void setConfirmedAt(String token);

}
