package com.example.addonisfinal.services.contracts;

import com.example.addonisfinal.models.Status;

public interface StatusService {

    Status getStatusByName(String name);

    void createStatus(Status statusName);

}
