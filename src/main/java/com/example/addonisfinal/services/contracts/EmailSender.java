package com.example.addonisfinal.services.contracts;

public interface EmailSender {

    void send(String to, String email);

    void sendContactForm(String from, String email);

    void sendInvite(String to, String email);

    void sendConfirmationCode(String to, String email);
}
