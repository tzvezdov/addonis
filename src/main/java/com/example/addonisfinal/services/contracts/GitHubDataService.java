package com.example.addonisfinal.services.contracts;

import com.example.addonisfinal.models.GitHubData;
import org.kohsuke.github.GitHub;

import java.io.IOException;
import java.util.Date;

public interface GitHubDataService {

    GitHubData save(GitHubData gitHubData);

    boolean validateLink(String url);

    GitHub getGitHubConnection() throws IOException;

    String getPullRequestCount(String url) throws IOException;

    String getOpenIssueCount(String url) throws IOException;

    Date getLastCommitDate(String url) throws IOException;

    String getLastCommitTitle(String url) throws IOException;
}
