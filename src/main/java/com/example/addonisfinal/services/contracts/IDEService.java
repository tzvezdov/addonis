package com.example.addonisfinal.services.contracts;

import com.example.addonisfinal.models.IDE;

import java.util.List;

public interface IDEService {

    List<IDE> getAll();

    IDE getByName(String ideName);

    void createIDE(IDE ide);

}
