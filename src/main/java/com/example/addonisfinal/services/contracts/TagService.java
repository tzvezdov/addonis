package com.example.addonisfinal.services.contracts;

import com.example.addonisfinal.models.Tag;

import java.util.List;

public interface TagService {

    Tag addTag(Tag tag);

    void deleteTag(Tag tag);

    List<Tag> getAll();

    Tag getByName(String name);

}
