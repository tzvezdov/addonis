package com.example.addonisfinal.services.contracts;


import com.example.addonisfinal.models.Role;

public interface RoleService {

    Role getByName(String name);

}
