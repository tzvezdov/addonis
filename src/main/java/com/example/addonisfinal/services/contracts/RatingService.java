package com.example.addonisfinal.services.contracts;

public interface RatingService {

    Double getAvgRatingOfAddon(int addonId);

}
