package com.example.addonisfinal.services.contracts;

import com.example.addonisfinal.models.ConfirmationCode;

import java.util.Optional;

public interface ConfirmationCodeService {

    void saveConfirmationCode(ConfirmationCode confirmationCode);

    Optional<ConfirmationCode> getCodeByAddonId(int id);

}
