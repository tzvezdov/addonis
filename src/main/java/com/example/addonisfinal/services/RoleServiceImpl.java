package com.example.addonisfinal.services;

import com.example.addonisfinal.exceptions.EntityNotFoundException;
import com.example.addonisfinal.models.Role;
import com.example.addonisfinal.repositories.RoleRepository;
import com.example.addonisfinal.services.contracts.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Override
    public Role getByName(String name) {
        Role role = roleRepository.findByName(name);

        if (role == null) {
            throw new EntityNotFoundException("Role", "name", name);
        }

        return role;
    }
}
