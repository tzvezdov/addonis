package com.example.addonisfinal.services;

import com.example.addonisfinal.exceptions.AlreadyRegisteredException;
import com.example.addonisfinal.exceptions.DuplicateEntityException;
import com.example.addonisfinal.exceptions.EntityNotFoundException;
import com.example.addonisfinal.exceptions.UnauthorizedOperationException;
import com.example.addonisfinal.models.AdditionalInfo;
import com.example.addonisfinal.models.ConfirmationToken;
import com.example.addonisfinal.models.Role;
import com.example.addonisfinal.models.User;
import com.example.addonisfinal.models.dtos.ContactUsDto;
import com.example.addonisfinal.models.dtos.user.InviteFriendDto;
import com.example.addonisfinal.models.dtos.user.UserEditPasswordDto;
import com.example.addonisfinal.repositories.RoleRepository;
import com.example.addonisfinal.repositories.UserRepository;
import com.example.addonisfinal.services.contracts.ConfirmationTokenService;
import com.example.addonisfinal.services.contracts.EmailSender;
import com.example.addonisfinal.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final EmailSender emailSender;
    private final ConfirmationTokenService tokenService;
    private final PasswordEncoder passwordEncoder;

    @Override
    public List<User> getAllUsers() {

        return userRepository.findAll();
    }

    @Override
    public List<User> getAllPendingVerificationUsers() {
        return userRepository.findUsersWhereCardAndSelfiesIsNotNullAndIsVerifiedFalseAndIsDeletedFalse();
    }

    @Override
    public List<User> getMaintainers(Integer addonId) {
        return userRepository.getMaintainers(addonId);
    }

    @Override
    public List<User> supplyUsers() {
        return userRepository.findAllByEnabledTrueAndDeletedFalse();
    }

    @Override
    public User getUsername(String username) {
        Optional<User> user = userRepository.findByUsername(username);

        if (user.isEmpty() || user.get().isDeleted()) {
            throw new EntityNotFoundException("User", "name", username);
        }
        return user.get();
    }

    @Override
    public User getById(int id) {

        User userToGet = userRepository.getUserById(id);

        if (userToGet == null || userToGet.isDeleted()) {
            throw new EntityNotFoundException("User", id);
        }

        return userToGet;
    }

    @Override
    public void create(User user) {

        if (userRepository.existsByUsername(user.getUsername())) {
            throw new DuplicateEntityException(
                    "User", "username", user.getUsername());
        }

        if (userRepository.existsByPhoneAndIdIsNot(user.getPhone(), 0)) {
            throw new DuplicateEntityException(
                    "Phone", "phone", user.getPhone());
        }

        if (userRepository.existsByEmailAndIdIsNot(user.getEmail(), 0)) {
            throw new DuplicateEntityException(
                    "Email", "email", user.getEmail());
        }

        AdditionalInfo additionalInfo = new AdditionalInfo();
        user.getRoles().add(roleRepository.findByName("USER"));
        user.setUserDetails(additionalInfo);
        user.setCreatedOn(LocalDate.now());
        userRepository.saveAndFlush(user);

        String token = UUID.randomUUID().toString();
        ConfirmationToken confirmationToken = new ConfirmationToken(token, LocalDateTime.now(), LocalDateTime.now().plusMinutes(15), user);
        tokenService.saveConfirmationToken(confirmationToken);

        String confirmationLink = "http://localhost:8080/api/users/confirm?token=" + token;
        emailSender.send(user.getEmail(), buildEmail(user.getFirstName(), confirmationLink));

    }

    @Override
    public void update(int id, User user, User userToBeChecked) {
        if (id != userToBeChecked.getId()) {
            throw new UnauthorizedOperationException("Only the profile owner can modify it.");
        }

        User userToUpdate = userRepository.getUserById(id);

        if (userRepository.existsByPhoneAndIdIsNot(user.getPhone(), id)) {
            throw new DuplicateEntityException(
                    "Phone", "phone", user.getPhone());
        }

        if (userRepository.existsByEmailAndIdIsNot(user.getEmail(), id)) {
            throw new DuplicateEntityException(
                    "Email", "email", user.getEmail());
        }

        userToUpdate.setEmail(user.getEmail());
        userToUpdate.setFirstName(user.getFirstName());
        userToUpdate.setLastName(user.getLastName());
        userToUpdate.setPhone(user.getPhone());

        userRepository.saveAndFlush(userToUpdate);
    }

    @Override
    public void changePassword(int id, UserEditPasswordDto dto, User userToBeChecked) {
        if (id != userToBeChecked.getId()) {
            throw new UnauthorizedOperationException("Only the profile owner can modify it.");
        }

        User userToUpdate = userRepository.getUserById(id);

        if (!passwordEncoder.matches(dto.getCurrentPassword(), userToBeChecked.getPassword())) {
            throw new UnauthorizedOperationException("Current password is incorrect!");
        }

        if (!dto.getNewPassword().equals(dto.getConfirmNewPassword())) {
            throw new UnauthorizedOperationException("New password and confirm new password should match!");
        }

        userToUpdate.setPassword(passwordEncoder.encode(dto.getNewPassword()));

        userRepository.saveAndFlush(userToUpdate);
    }

    @Override
    public void uploadAvatar(int id, User userToBeChecked, MultipartFile multipartFile) throws IOException {
        User userToUpdate = userRepository.getUserById(id);

        if (id != userToBeChecked.getId()) {
            throw new UnauthorizedOperationException("Only the profile owner can modify it.");
        }


        if (multipartFile != null && multipartFile.getSize() > 0) {
            userToUpdate.setAvatar(Base64.getEncoder().encodeToString(multipartFile.getBytes()));
        }

        userRepository.saveAndFlush(userToUpdate);
    }

    @Override
    public void uploadVerificationPictures(int id, User userToBeChecked, MultipartFile selfie, MultipartFile idCard) throws IOException {
        User userToUpdate = userRepository.getUserById(id);

        if (id != userToBeChecked.getId()) {
            throw new UnauthorizedOperationException("Only the profile owner can modify it.");
        }


        if (selfie != null && selfie.getSize() > 0) {
            userToUpdate.getUserDetails().setSelfie(Base64.getEncoder().encodeToString(selfie.getBytes()));
        }

        if (idCard != null && idCard.getSize() > 0) {
            userToUpdate.getUserDetails().setIdCard(Base64.getEncoder().encodeToString(idCard.getBytes()));
        }

        userRepository.saveAndFlush(userToUpdate);
    }

    @Override
    public void delete(int id, int userToBeCheckedId) {
        if (id != userToBeCheckedId) {
            throw new UnauthorizedOperationException("Only the profile owner can delete it.");
        }

        User user = userRepository.getUserById(id);
        user.setDeleted(true);
        user.getUserDetails().setDeletedOn(LocalDate.now());

        userRepository.saveAndFlush(user);
    }

    @Override
    public void changePermissions(int id, User admin) {
        if (!admin.isAdmin()) {
            throw new UnauthorizedOperationException("Only admins can change permissions");
        }
        User userToBeChanged = userRepository.getUserById(id);
        if (!userToBeChanged.isAdmin()) {
            setAdmin(userToBeChanged);
        } else {
            demoteAdmin(userToBeChanged);
        }

        userRepository.saveAndFlush(userToBeChanged);
    }

    @Override
    public void block(int userId, User admin) {
        if (!admin.isAdmin()) {
            throw new UnauthorizedOperationException("Only admins can block users.");
        }

        User user = userRepository.getUserById(userId);
        user.getUserDetails().setBlocked(true);
        user.getUserDetails().setBlockedOn(LocalDate.now());

        userRepository.saveAndFlush(user);
    }

    @Override
    public void unblock(int userId, User admin) {
        if (!admin.isAdmin()) {
            throw new UnauthorizedOperationException("Only admins can unblock users.");
        }

        User user = userRepository.getUserById(userId);
        user.getUserDetails().setBlocked(false);
        user.getUserDetails().setBlockedOn(null);

        userRepository.saveAndFlush(user);

    }

    @Override
    public void verify(int userId, User admin) {
        if (!admin.isAdmin()) {
            throw new UnauthorizedOperationException("Only admins can verify users.");
        }

        User user = userRepository.getUserById(userId);
        user.getUserDetails().setVerified(true);

        userRepository.saveAndFlush(user);
    }

    @Override
    public void inviteFriend(User user, String email) {
        if (userRepository.existsByEmail(email)) {
            throw new AlreadyRegisteredException(email);
        }

        String invitationLink = "http://localhost:8080/api/users/create";

        emailSender.sendInvite(email, buildInviteEmail(user.getUsername(), invitationLink));
    }

    @Override
    public Page<User> getAllByMultipleFilters(String username, int pageNumber, int maxResultsPerPage) {
        Pageable pageable = PageRequest.of(pageNumber - 1, maxResultsPerPage);
        return userRepository.findDistinctByEnabledIsTrueAndUsernameLikeAndDeletedIsFalse(username,
                pageable);
    }

    @Override
    public void resendToken(int id, InviteFriendDto user) {

        User userToBeChecked = userRepository.getUserById(id);

        if (!user.getEmail().equals(userToBeChecked.getEmail())) {
            throw new UnauthorizedOperationException("Only the profile owner can request new token");
        }

        if (userToBeChecked.isEnabled()) {
            throw new DuplicateEntityException("Token", "this", "number");
        }

        String token = UUID.randomUUID().toString();
        ConfirmationToken confirmationToken = new ConfirmationToken(token, LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(15), userToBeChecked);
        tokenService.saveConfirmationToken(confirmationToken);

        String confirmationLink = "http://localhost:8080/api/users/confirm?token=" + token;
        emailSender.send(userToBeChecked.getEmail(), buildEmail(userToBeChecked.getFirstName(), confirmationLink));
    }

    private void demoteAdmin(User user) {
        Role role = roleRepository.findByName("admin");
        user.getRoles().remove(role);
    }

    private void setAdmin(User user) {
        Role role = roleRepository.findByName("admin");
        user.getRoles().add(role);
    }


    @Override
    @Transactional
    public void confirmToken(String token) {

        ConfirmationToken confirmationToken = tokenService.getToken(token)
                .orElseThrow(() -> new IllegalStateException("Token not found."));

        if (confirmationToken.getConfirmedAt() != null) {
            throw new IllegalStateException("Email already confirmed.");
        }

        LocalDateTime expiredAt = confirmationToken.getExpiresAt();

        if (expiredAt.isBefore(LocalDateTime.now())) {
            throw new IllegalStateException("Token expired.");
        }

        tokenService.setConfirmedAt(token);
        User user = userRepository.findByToken(token);
        userRepository.enableUser(user.getUsername());

        System.out.println("CONFIRMED TOKEN");
    }

    @Override
    public void sendContactUsEmail(ContactUsDto contactUsDto) {

        emailSender.sendContactForm(contactUsDto.getEmail(), buildContactUsEmail(contactUsDto));
    }


    private String buildEmail(String name, String link) {
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
                "\n" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                "\n" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                "        \n" +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                "          <tbody><tr>\n" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td style=\"padding-left:10px\">\n" +
                "                  \n" +
                "                    </td>\n" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirm your email</span>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "              </a>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                "      <td>\n" +
                "        \n" +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "\n" +
                "\n" +
                "\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> Thank you for registering. Please click on the below link to activate your account: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\"" + link + "\">Activate Now</a> </p></blockquote>\n Link will expire in 15 minutes. <p>See you soon</p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                "\n" +
                "</div></div>";
    }

    private String buildInviteEmail(String sender, String link) {
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
                "\n" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                "\n" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                "        \n" +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                "          <tbody><tr>\n" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td style=\"padding-left:10px\">\n" +
                "                  \n" +
                "                    </td>\n" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Did you know we are the best addons registry?</span>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "              </a>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                "      <td>\n" +
                "        \n" +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "\n" +
                "\n" +
                "\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + "Friend" + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> It seems that you have friends using our platforms and they want to invite you to experience it as well. Please click on the below link to register and join our family: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\"" + link + "\">Register</a> </p></blockquote>\n <p>See you soon</p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                "\n" +
                "</div></div>";

    }

    private String buildContactUsEmail(ContactUsDto contactUsDto) {
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
                "\n" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                "\n" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                "        \n" +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                "          <tbody><tr>\n" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td style=\"padding-left:10px\">\n" +
                "                  \n" +
                "                    </td>\n" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Contact us form</span>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "              </a>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                "      <td>\n" +
                "        \n" +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "\n" +
                "\n" +
                "\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + "Addonis Team" + "user with email: " + contactUsDto.getEmail() + "is contacting you: " + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> " + contactUsDto.getMessage() + " </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> </p></blockquote>\n <p>Hope for a quick response! </p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                "\n" +
                "</div></div>";
    }
}
