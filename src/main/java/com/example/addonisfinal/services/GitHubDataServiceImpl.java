package com.example.addonisfinal.services;

import com.example.addonisfinal.models.GitHubData;
import com.example.addonisfinal.repositories.GitHubDataRepository;
import com.example.addonisfinal.services.contracts.GitHubDataService;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.kohsuke.github.GHIssueState;
import org.kohsuke.github.GitHub;
import org.kohsuke.github.GitHubBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.IOException;
import java.util.Date;

@Service
@RequiredArgsConstructor
public class GitHubDataServiceImpl implements GitHubDataService {

    private final GitHubDataRepository gitHubDataRepository;
    public static final String GITHUB_PREFIX = "https://github.com/";
    public static final String GITHUB_TOKEN = "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";
    WebClient.Builder builder = WebClient.builder().baseUrl("https://api.github.com/repos");

    @Override
    public GitHubData save(GitHubData gitHubData) {
        return gitHubDataRepository.save(gitHubData);
    }

    public boolean validateLink(String url) {

        if (!url.startsWith(GITHUB_PREFIX)) {

            return false;
        }

        url = url.substring(GITHUB_PREFIX.length());
        String[] remainder = url.split("/");
        return remainder.length >= 2;
    }

    @Override
    public GitHub getGitHubConnection() throws IOException {

        return new GitHubBuilder().withOAuthToken(GITHUB_TOKEN).build();
    }

    @Override
    public String getPullRequestCount(String url) throws IOException {
        GitHub gitHubConnection = getGitHubConnection();
        String pullsCount = String.valueOf(gitHubConnection.getRepository(url).getPullRequests(GHIssueState.OPEN).size());

        return pullsCount;
    }

    @Override
    public String getOpenIssueCount(String url) throws IOException {

        GitHub gitHubConnection = getGitHubConnection();
        String issueCount = String.valueOf(gitHubConnection.getRepository(url).getOpenIssueCount());

        return issueCount;
    }

    @Override
    public Date getLastCommitDate(String url) throws IOException {
        String[] ownerAndRepo = url.split("/");

        String rawData = webClientConnection(ownerAndRepo);

        return getCommitDateFromJSON(rawData);
    }

    @Override
    public String getLastCommitTitle(String url) throws IOException {

        String[] ownerAndRepo = url.split("/");

        String rawData = webClientConnection(ownerAndRepo);

        return getCommitMessageFromJSON(rawData);
    }

    private String webClientConnection(String[] ownerAndRepo) {
        return builder.build()
                .get().uri(String.format("/%s/%s/commits?per_page=1&page=1", ownerAndRepo[0], ownerAndRepo[1]))
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }

    private String getCommitMessageFromJSON(String rawJson) throws JSONException {
        JSONArray arr = new JSONArray(rawJson);
        JSONObject commit = (JSONObject) new JSONObject(arr.get(0).toString()).get("commit");

        return commit.get("message").toString().strip();
    }

    private Date getCommitDateFromJSON(String rawJson) {

        JSONArray array = new JSONArray(rawJson);
        JSONObject commit = (JSONObject) new JSONObject(array.get(0).toString()).get("commit");
        String date = commit.getJSONObject("author").get("date").toString().split("T")[0];
        return java.sql.Date.valueOf(date);
    }
}
