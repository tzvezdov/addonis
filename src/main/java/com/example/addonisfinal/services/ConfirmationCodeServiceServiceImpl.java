package com.example.addonisfinal.services;

import com.example.addonisfinal.models.ConfirmationCode;
import com.example.addonisfinal.repositories.ConfirmationCodeRepository;
import com.example.addonisfinal.services.contracts.ConfirmationCodeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ConfirmationCodeServiceServiceImpl implements ConfirmationCodeService {

    private final ConfirmationCodeRepository codeRepository;


    @Override
    public void saveConfirmationCode(ConfirmationCode confirmationCode) {
        codeRepository.saveAndFlush(confirmationCode);
    }

    @Override
    public Optional<ConfirmationCode> getCodeByAddonId(int id) {
        return codeRepository.findByAddon_Id(id);
    }
}
