package com.example.addonisfinal.services;

import com.example.addonisfinal.exceptions.EntityNotFoundException;
import com.example.addonisfinal.models.IDE;
import com.example.addonisfinal.repositories.IDERepository;
import com.example.addonisfinal.services.contracts.IDEService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class IDEServiceImpl implements IDEService {
    private final IDERepository ideRepository;

    @Override
    public List<IDE> getAll() {
        return ideRepository.findAll();
    }

    @Override
    public IDE getByName(String ideName) {
        Optional<IDE> ide = ideRepository.findByName(ideName);

        return ide.orElseThrow(() -> new EntityNotFoundException(IDE.class.getSimpleName()));
    }

    @Override
    public void createIDE(IDE ide) {
        ideRepository.saveAndFlush(ide);
    }
}
