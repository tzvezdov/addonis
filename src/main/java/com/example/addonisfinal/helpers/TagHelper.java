package com.example.addonisfinal.helpers;


import com.example.addonisfinal.exceptions.EntityNotFoundException;
import com.example.addonisfinal.models.Tag;
import com.example.addonisfinal.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class TagHelper {

    private final TagService tagService;

    @Autowired
    public TagHelper(TagService tagService) {
        this.tagService = tagService;
    }

    public Set<Tag> prepareTags(String tags) {


        Set<Tag> postTags = new HashSet<>();

        if (tags == null) {
            return postTags;
        }

        tags = tags.replaceAll("#", "");
        String[] arrayTags = tags.trim().split(",");

        if (arrayTags.length == 0) {
            return postTags;
        } else {

            for (String tag : arrayTags) {

                try {
                    Tag existingTag = tagService.getByName(tag.trim());
                    postTags.add(existingTag);
                } catch (EntityNotFoundException | IllegalArgumentException e) {
                    Tag newTag = new Tag();
                    newTag.setName(tag);
                    postTags.add(newTag);
                    tagService.addTag(newTag);
                }
            }
        }
        return postTags;
    }
}
