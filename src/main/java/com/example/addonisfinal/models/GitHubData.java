package com.example.addonisfinal.models;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "github_data")
@NoArgsConstructor
@Getter
@Setter
public class GitHubData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "github_data_id", nullable = false, unique = true, updatable = false)
    private Long id;

    @Column(name = "open_issues_count")
    private String openIssuesCount;

    @Column(name = "pulls_count")
    private String pullCount;

    @Column(name = "last_commit_title", length = 1000)
    private String lastCommitTitle;

    @Column(name = "last_commit_date")
    private Date lastCommitDate;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;


}
