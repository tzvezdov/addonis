package com.example.addonisfinal.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(name = "UniqueAddonIdAndUserId", columnNames = {"addon_id", "user_id"}))
@NoArgsConstructor
@Getter
@Setter
public class JointAddon {

    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    @JoinColumn(name = "addon_id")
    private Addon addon;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

}
