package com.example.addonisfinal.models;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "confirmation_code")
public class ConfirmationCode {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true, updatable = false)
    private int id;

    @Column(name = "code", nullable = false)
    private String code;

    @ManyToOne(targetEntity = Addon.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "addon_id")
    private Addon addon;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public ConfirmationCode(String code, Addon addon, User user) {
        this.code = code;
        this.addon = addon;
        this.user = user;
    }
}
