package com.example.addonisfinal.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "additional_info")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AdditionalInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true, updatable = false)
    private int id;

    @Lob
    @Column(name = "id_card")
    private String idCard;

    @Lob
    @Column(name = "selfie")
    private String selfie;


    @Column(name = "deleted_on")
    private LocalDate deletedOn;

    @Column(name = "is_blocked", columnDefinition = "boolean default false", nullable = false)
    private boolean isBlocked = false;

    @Column(name = "blocked_on")
    private LocalDate blockedOn;

    @Column(name = "is_verified", columnDefinition = "boolean default false", nullable = false)
    private boolean isVerified = false;


}
