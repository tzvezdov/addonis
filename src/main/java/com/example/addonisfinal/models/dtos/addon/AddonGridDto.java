package com.example.addonisfinal.models.dtos.addon;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AddonGridDto {
    private int id;
    private String name;
    private String avatar;
    private String description;
    private String ide;
    private String creatorName;
    private String creatorAvatar;
    private String downloadLink;
    private double rating;
}
