package com.example.addonisfinal.models.dtos.user;

import com.example.addonisfinal.models.AdditionalInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Setter
@Getter
public class UserUpdateDto {

    int id;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String avatar;
    private String username;
    private AdditionalInfo additionalInfo;
}
