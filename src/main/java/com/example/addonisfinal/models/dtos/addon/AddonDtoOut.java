package com.example.addonisfinal.models.dtos.addon;

import com.example.addonisfinal.models.Tag;
import com.example.addonisfinal.models.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class AddonDtoOut {

    private String name;
    private String description;
    private String creator;
    private String status;
    private String ide;
    private LocalDateTime createdOn;
    private LocalDateTime lastModifiedOn;

    private List<User> maintainers;

    private Set<Tag> tags;


    private double rating;


}
