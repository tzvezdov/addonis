package com.example.addonisfinal.models.dtos.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@NoArgsConstructor
@Setter
@Getter
public class UserEditPasswordDto extends UserUpdateDto {

    private int id;

    @Size(min = 6, max = 68, message = "Password must be between {min} and {max} characters")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$")
    @NotEmpty(message = "Password shouldn't be empty")
    private String currentPassword;

    @Size(min = 6, max = 68, message = "Password must be between {min} and {max} characters")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$")
    @NotEmpty(message = "Password shouldn't be empty")
    private String newPassword;

    @Size(min = 6, max = 68, message = "Password must be between {min} and {max} characters")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$")
    @NotEmpty(message = "Password shouldn't be empty")
    private String confirmNewPassword;

}
