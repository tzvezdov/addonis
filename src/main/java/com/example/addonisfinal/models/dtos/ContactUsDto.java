package com.example.addonisfinal.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContactUsDto {

    @NotEmpty
    private String name;

    @NotEmpty
    private String email;

    @NotEmpty
    @Size(min = 5, max = 150)
    private String subject;

    @NotEmpty
    @Size(min = 10, max = 1024)
    private String message;

}
