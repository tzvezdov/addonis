package com.example.addonisfinal.models.dtos.addon;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@NoArgsConstructor
@Setter
@Getter
public class AddonDto {

    private int id;

    @Size(min = 3, max = 50, message = "Name of addon must be between 3 and 50 characters")
    private String name;

    @Size(min = 3, max = 5000, message = "Description must be between 3 and 5000 characters")
    private String description;

    @NotEmpty(message = "Please select at lest 1 tag")
    private String tags;

    @NotEmpty
    private String ideName;

    @NotEmpty
    @Pattern(regexp = "^.*github.*$", message = "Please make sure you are adding a GitHub link")
    private String originLink;

    private MultipartFile file;

    private MultipartFile image;
}
