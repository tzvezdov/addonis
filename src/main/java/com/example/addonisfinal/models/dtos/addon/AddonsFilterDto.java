package com.example.addonisfinal.models.dtos.addon;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class AddonsFilterDto {


    private String addonName = "";
    private String creatorUsername = "";
    private String tags = "";
    private String ideName = "";
    private String orderBy = "";
    private String emptyString = "";

    public static String order(String orderBy) {
        return orderBy;
    }

}
