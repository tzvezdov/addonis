package com.example.addonisfinal.models.dtos.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class RestUserDetailsDto {

    private int id;
    private String username;
    private String email;
    private String firstName;
    private String lastName;
    private String phone;
}
