package com.example.addonisfinal.models.dtos.authentication;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@NoArgsConstructor
@Setter
@Getter
public class RegisterDto extends LoginDto {

    @NotEmpty(message = "Phone name can't be empty")
    @Length(min = 10, max = 10, message = "Phone must be {min} characters long")
    private String phone;

    @Size(min = 6, max = 68, message = "Password must be between {min} and {max} characters")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$", message = "Must contain at least one  number and one uppercase and lowercase letter, and at least 4 or more characters")
    @NotEmpty(message = "Password shouldn't be empty")
    private String passwordConfirm;

    @NotEmpty(message = "First name can't be empty")
    @Size(min = 3, max = 20, message = "First name must be between 3 and 20 characters")
    private String firstName;

    @NotEmpty(message = "Last name can't be empty")
    @Size(min = 3, max = 20, message = "Last name must be between 3 and 20 characters")
    private String lastName;

    @Pattern(regexp = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$")
    private String email;

}
