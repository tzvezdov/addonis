package com.example.addonisfinal.models.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RatingDto {

    Double avgRating;
    Integer vote;
}
