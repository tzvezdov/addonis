package com.example.addonisfinal.models.dtos.addon;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CodeInputDto {

    @NotEmpty
    @Size(min = 6,max = 6)
    private String code;

}
