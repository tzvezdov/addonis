package com.example.addonisfinal.models.dtos.authentication;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@NoArgsConstructor
@Setter
@Getter
public class LoginDto {

    @NotEmpty(message = "Username shouldn't be empty")
    @Size(min = 3, max = 50, message = "Username must be between {min} and {max} characters")
    private String username;

    @Size(min = 6, max = 68, message = "Password must be between {min} and {max} characters")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$")
    @NotEmpty(message = "Password shouldn't be empty")
    private String password;

}
