package com.example.addonisfinal.models.dtos.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@NoArgsConstructor
@Getter
@Setter
public class UserVerifyDto {

    private MultipartFile selfie;
    private MultipartFile idCard;
}
