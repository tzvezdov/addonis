package com.example.addonisfinal.models.mappers;

import com.example.addonisfinal.models.User;
import com.example.addonisfinal.models.dtos.authentication.RegisterDto;
import com.example.addonisfinal.models.dtos.user.RestUserDetailsDto;
import com.example.addonisfinal.models.dtos.user.UserUpdateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserMapper(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public User userToObjectRegistration(RegisterDto userDtoOut) {

        User user = new User();
        user.setPassword(passwordEncoder.encode(userDtoOut.getPassword()));
        user.setUsername(userDtoOut.getUsername());
        user.setEmail(userDtoOut.getEmail());
        user.setFirstName(userDtoOut.getFirstName());
        user.setLastName(userDtoOut.getLastName());
        user.setPhone(userDtoOut.getPhone());

        return user;
    }

    public RestUserDetailsDto userToObject(User user) {

        RestUserDetailsDto registerDto = new RestUserDetailsDto();

        registerDto.setEmail(user.getEmail());
        registerDto.setId(user.getId());
        registerDto.setFirstName(user.getFirstName());
        registerDto.setLastName(user.getLastName());
        registerDto.setUsername(user.getUsername());
        registerDto.setPhone(user.getPhone());

        return registerDto;
    }

    public UserUpdateDto fromUserToUserUpdateDto(User user) {
        UserUpdateDto updateDto = new UserUpdateDto();
        updateDto.setEmail(user.getEmail());
        updateDto.setId(user.getId());
        updateDto.setPhone(user.getPhone());
        updateDto.setFirstName(user.getFirstName());
        updateDto.setLastName(user.getLastName());
        updateDto.setAvatar(user.getAvatar());
        updateDto.setUsername(user.getUsername());
        updateDto.setAdditionalInfo(user.getUserDetails());

        return updateDto;
    }

    public User fromUserUpdateDtoToUser(UserUpdateDto userUpdateDto) {
        User user = new User();
        user.setEmail(userUpdateDto.getEmail());
        user.setPhone(userUpdateDto.getPhone());
        user.setFirstName(userUpdateDto.getFirstName());
        user.setLastName(userUpdateDto.getLastName());
        user.setId(userUpdateDto.getId());
        user.setUserDetails(userUpdateDto.getAdditionalInfo());

        return user;
    }
}
