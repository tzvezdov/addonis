package com.example.addonisfinal.models.mappers;

import com.example.addonisfinal.exceptions.UnauthorizedOperationException;
import com.example.addonisfinal.helpers.TagHelper;
import com.example.addonisfinal.models.Addon;
import com.example.addonisfinal.models.Rating;
import com.example.addonisfinal.models.User;
import com.example.addonisfinal.models.dtos.addon.AddonDto;
import com.example.addonisfinal.models.dtos.addon.AddonDtoOut;
import com.example.addonisfinal.models.dtos.addon.AddonGridDto;
import com.example.addonisfinal.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.util.Base64;

@Component
@RequiredArgsConstructor
public class AddonMapper {

    private final TagHelper tagHelper;
    private final UserService userService;


    public Addon dtoToAddon(AddonDto addonDto) throws IOException {

        Addon addon = new Addon();
        addon.setName(addonDto.getName());
        addon.setDescription(addonDto.getDescription());
        addon.setTags(tagHelper.prepareTags(addonDto.getTags()));
        addon.setFile(Base64.getEncoder().encode(addonDto.getFile().getBytes()));

        return addon;
    }

    public AddonDtoOut objectToDtoOut(Addon addon) {

        AddonDtoOut addonDtoOut = new AddonDtoOut();

        addonDtoOut.setName(addon.getName());
        addonDtoOut.setDescription(addon.getDescription());
        addonDtoOut.setCreator(addon.getCreator().getUsername());
        addonDtoOut.setStatus(addon.getStatus().getName());
        addonDtoOut.setIde(addon.getIde().getName());
        addonDtoOut.setCreatedOn(addon.getCreatedOn());
        addonDtoOut.setLastModifiedOn(addon.getLastModifiedOn());
        addonDtoOut.setTags(addon.getTags());
        addonDtoOut.setMaintainers(userService.getMaintainers(addon.getId()));

        return addonDtoOut;
    }

    public AddonGridDto objectToGridDto(Addon addon) {

        AddonGridDto addonDtoOut = new AddonGridDto();

        addonDtoOut.setName(addon.getName());
        addonDtoOut.setIde(addon.getIde().getName());
        addonDtoOut.setId(addon.getId());
        addonDtoOut.setCreatorName(addon.getCreator().getUsername());
        addonDtoOut.setCreatorAvatar(addon.getCreator().getAvatar());
        addonDtoOut.setAvatar(addon.getAvatar());
        addonDtoOut.setDescription(addon.getDescription());
        addonDtoOut.setDownloadLink(addon.getDownloadLink());

        if (addon.getRating().isEmpty()) {
            addonDtoOut.setRating(0);
        } else {
            var rating = addon.getRating().stream().mapToInt(Rating::getRating).average();
            addonDtoOut.setRating(Double.parseDouble(String.format("%.1f", rating.getAsDouble())));
        }

        return addonDtoOut;
    }

    public void transferDetails(Addon addon, AddonDto newInput) {

        newInput.setName(addon.getName());
        newInput.setId(addon.getId());
        newInput.setDescription(addon.getDescription());
        newInput.setOriginLink(addon.getOriginLink());
        newInput.setIdeName(addon.getIde().getName());
    }

    public Addon setDownloadLinkToAddon(User userToBeChecked, Addon addon, MultipartFile file) {

        if (!userToBeChecked.equals(addon.getCreator())) {
            throw new UnauthorizedOperationException("You must be the owner of the addon in order to upload a file.");
        }

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/download/")
                .path(String.valueOf(addon.getId()))
                .path("/")
                .toUriString();
        addon.setDownloadLink(fileDownloadUri);
        addon.setFileName(fileName);

        return addon;
    }

}
