package com.example.addonisfinal.models.mappers;

import com.example.addonisfinal.models.Rating;
import com.example.addonisfinal.models.dtos.RatingDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RatingMapper {

    public Rating mapRatingDtoToRating(RatingDto ratingDTO){

        Rating rating = new Rating();
        rating.setRating(ratingDTO.getVote());

        return rating;
    }
}
