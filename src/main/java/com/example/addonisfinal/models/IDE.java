package com.example.addonisfinal.models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "ides")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class IDE {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true, updatable = false)
    private int id;

    @Column(name = "name")
    private String name;

    @Override
    public String toString() {
        return name;
    }
}
