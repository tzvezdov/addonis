package com.example.addonisfinal.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "addons")
@NoArgsConstructor
@Getter
@Setter
public class Addon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true, updatable = false)
    private int id;

    @Column(name = "name", nullable = false)
    @Size(min = 3, max = 50, message = "Name of addon must be between 3 and 50 characters")
    private String name;

    @Column(name = "description", nullable = false)
    @Size(min = 3, max = 5000, message = "Description must be between 3 and 5000 characters")
    private String description;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User creator;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "status_id", nullable = false)
    private Status status;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ide_id", nullable = false)
    private IDE ide;

    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @Column(name = "is_deleted", nullable = false)
    private boolean deleted;

    @Column(name = "modified_on")
    private LocalDateTime lastModifiedOn;

    @Lob
    @Column(name = "binary_file")
    @JsonIgnore
    private byte[] file;

    @Lob
    @Column(name = "avatar")
    private String avatar;

    private String fileName;

    @Column(name = "downloads_count", nullable = false)
    private int downloadsCount;

    @Column(name = "download_link")
    private String downloadLink;

    @Column(name = "origin_link")
    private String originLink;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "addons_tags",
            joinColumns = @JoinColumn(name = "addon_id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "tag_id", nullable = false)
    )
    private Set<Tag> tags = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "addon", fetch = FetchType.EAGER)
    private Set<Rating> rating;


    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "git_hub_data_id", nullable = false)
    private GitHubData gitHubData;

    @Column
    private String maintainers = "";

    @Column(name = "is_confirmed", nullable = false)
    private boolean confirmed;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof Addon)) {
            return false;
        }

        Addon post = (Addon) obj;

        return this.getId() == post.getId();
    }
}
