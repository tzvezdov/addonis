package com.example.addonisfinal.models.filters;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
public class AddonisFilter {
    private String addonName;
    private String creatorUsername;
    private String ideName;
    private String tags;
    private String orderBy;


}
