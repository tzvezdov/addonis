package com.example.addonisfinal.exceptions;

public class EntityNotFoundException extends RuntimeException{

    public EntityNotFoundException(String type, int id) {
        this(type, "id", String.valueOf(id));
    }
    public EntityNotFoundException(String type) {
        super(String.format("Entity with type %s was not found.", type));
    }
    public EntityNotFoundException(String type, String attribute, String value) {
        super(String.format("%s with %s %s not found", type,attribute,value));
    }
}
