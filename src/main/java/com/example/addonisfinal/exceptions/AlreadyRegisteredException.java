package com.example.addonisfinal.exceptions;

public class AlreadyRegisteredException extends RuntimeException {

    public AlreadyRegisteredException(String email) {
        super(String.format("User with email %s already exists.", email));
    }
}
