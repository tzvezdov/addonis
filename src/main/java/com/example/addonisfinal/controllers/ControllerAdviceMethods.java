package com.example.addonisfinal.controllers;

import com.example.addonisfinal.models.User;
import com.example.addonisfinal.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.List;

@ControllerAdvice
@RequestMapping
@RequiredArgsConstructor
public class ControllerAdviceMethods {

    private final UserService userService;

    @ModelAttribute("currentUserIsAdmin")
    public boolean currentUserIsAdmin(Principal principal) {

        if (principal != null) {
            User user = userService.getUsername(principal.getName());
            return user.isAdmin();
        }
        return false;
    }

    @ModelAttribute("currentUserIsLoggedIn")
    public boolean userIsLoggedIn(Principal principal) {
        return principal != null;
    }

    @ModelAttribute("users")
    public List<User> supplyUsersForMaintainers(Principal principal) {

        if (principal != null) {
            List<User> list = userService.supplyUsers();
            list.remove(userService.getUsername(principal.getName()));
            return list;
        } else
            return userService.supplyUsers();
    }

    @ModelAttribute("currentUserId")
    public int currentUser(Principal principal) {
        if (principal != null) {
            User user = userService.getUsername(principal.getName());
            return user.getId();
        }
        return 0;
    }

    @ModelAttribute("userObject")
    public User user(Principal principal) {
        if (principal != null) {
            User user = userService.getUsername(principal.getName());
            return user;
        }
        return null;
    }
}
