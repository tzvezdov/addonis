package com.example.addonisfinal.controllers.mvc;

import com.example.addonisfinal.exceptions.DuplicateEntityException;
import com.example.addonisfinal.exceptions.EntityNotFoundException;
import com.example.addonisfinal.helpers.TagHelper;
import com.example.addonisfinal.models.*;
import com.example.addonisfinal.models.dtos.*;
import com.example.addonisfinal.models.dtos.addon.AddonDto;
import com.example.addonisfinal.models.dtos.addon.AddonGridDto;
import com.example.addonisfinal.models.dtos.addon.AddonsFilterDto;
import com.example.addonisfinal.models.mappers.AddonMapper;
import com.example.addonisfinal.models.mappers.RatingMapper;
import com.example.addonisfinal.services.contracts.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/addons")
@RequiredArgsConstructor
public class AddonsMvcController {

    private final AddonService addonService;
    private final UserService userService;
    private final AddonMapper mapper;
    private final IDEService ideService;
    private final TagService tagService;
    private final RatingService ratingService;

    private final RatingMapper ratingMapper;

    @ModelAttribute("ides")
    public List<IDE> supplyIDEs() {
        return ideService.getAll();
    }

    @ModelAttribute("tags")
    public List<Tag> supplyTags() {
        return tagService.getAll();
    }

    @ModelAttribute("filterSortDTO")
    public AddonsFilterDto supplyFilterSortDTO() {
        return new AddonsFilterDto();
    }

    @GetMapping("/filter/{page}")
    public String multipleFilterOfAddons(@ModelAttribute ("filterSortDTO") AddonsFilterDto filterSortDTO,
                                         Model model,
                                         @PathVariable int page
    ) {

        Page<Addon> addons = addonService.getAllByMultipleFilters(
                "%" + filterSortDTO.getAddonName() + "%",
                "%" + filterSortDTO.getCreatorUsername() + "%",
                "%" + filterSortDTO.getTags() + "%",
                "%" + filterSortDTO.getIdeName() + "%",
                filterSortDTO.getOrderBy(),
                page,
                6);

        List<AddonGridDto> addonsResult = new ArrayList<>();
        for (Addon addon : addons.getContent()) {
            addonsResult.add(mapper.objectToGridDto(addon));
        }
        model.addAttribute("addons", addonsResult);
        model = supplyPageInformation(model, page, addons);

        return "addons";
    }


    @GetMapping("/{id}")
    public String showSingleAddon(@PathVariable int id, Model model) {

        try {
            Addon addon = addonService.getById(id);
            List<String> tags = addon.getTags().stream().map(Tag::getName).collect(Collectors.toList());
            RatingDto ratingDTO = new RatingDto();

            if (addon.getRating().isEmpty()) {
                ratingDTO.setAvgRating(0.0);
            } else {
                ratingDTO.setAvgRating(Double.valueOf(String.format("%.1f", ratingService.getAvgRatingOfAddon(addon.getId()))));
            }

            model.addAttribute("rating", ratingDTO);
            model.addAttribute("addon", addon);
            model.addAttribute("tags", tags);


        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "index";
        }

        return "single-addon";
    }


    private Model supplyPageInformation(Model model, int page, Page<Addon> addons) {
        if (addons.hasPrevious()) {
            model.addAttribute("firstPage", false);
        }
        if (addons.hasNext()) {
            model.addAttribute("lastPage", false);
        }

        model.addAttribute("page", page);

        return model;
    }

    @GetMapping("/user/{userId}")
    public String showUserAddons(@PathVariable int userId, Model model) {
        User user = userService.getById(userId);
        List<Addon> addons = addonService.getByUser(userService.getById(userId));
        model.addAttribute("userAddons", addons);
        model.addAttribute("user", user);
        return "user-addons";
    }

    @PreAuthorize("hasAnyAuthority('user','admin')")
    @PostMapping("delete/{id}")
    public String deleteAddon(@PathVariable int id, Principal principal) {

        User currentUser = userService.getUsername(principal.getName());

        addonService.delete(currentUser, id);

        return "addons";
    }

    @PreAuthorize("hasAnyAuthority('user','admin')")
    @GetMapping("{id}/edit")
    public String showUpdatePage(@PathVariable int id, Model model) {

        try {
            Addon addon = addonService.getById(id);
            AddonDto newInput = new AddonDto();
            mapper.transferDetails(addon,newInput);

            model.addAttribute("oldData", addon);
            model.addAttribute("newInput", newInput);
            return "update-addon-view";
        } catch (EntityNotFoundException e) {
            return "not-found";  // Change to not-found view later.
        }
    }

    @PostMapping("{id}/edit")
    public String updateAddon(@ModelAttribute("newInput") AddonDto addonToUpdate, Principal principal, @PathVariable int id, Model model) throws IOException {

        try {

            User userToBeChecked = userService.getUsername(principal.getName());
            Addon addon = addonService.getById(id);

            addonService.updateAddon(addon, addonToUpdate, userToBeChecked);

            return "redirect:/addons/{id}";
        } catch (IOException | EntityNotFoundException | DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "update-addon-view"; // check error handling later
        }
    }

    @PostMapping("/{id}/rate")
    public String rate(@ModelAttribute RatingDto ratingDTO, @PathVariable int id, Principal principal) {
        try {
            User userToBeChecked = userService.getUsername(principal.getName());
            Addon addon = addonService.getById(id);

            Rating rating = ratingMapper.mapRatingDtoToRating(ratingDTO);

            addonService.rateAddon(addon.getId(), userToBeChecked, rating);

            return "redirect:/addons/{id}";
        } catch (EntityNotFoundException e) {
            return "addons";
        }
    }

    @GetMapping("/verify-my-addons")
    public String openVerifyAddonsPage(Principal principal, Model model) {

        User loggedInUser = userService.getUsername(principal.getName());
        List<Addon> unverifiedAddons = addonService.findUnverifiedAddonsForUser(loggedInUser.getId());
        unverifiedAddons = unverifiedAddons.stream()
                .filter(addon -> addon.getStatus().getName().equals("unconfirmed")).collect(Collectors.toList());

        model.addAttribute("unverifiedAddons", unverifiedAddons);

        return "verify-my-addons";
    }

}
