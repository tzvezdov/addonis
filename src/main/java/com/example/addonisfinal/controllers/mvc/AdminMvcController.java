package com.example.addonisfinal.controllers.mvc;

import com.example.addonisfinal.models.Addon;
import com.example.addonisfinal.models.User;
import com.example.addonisfinal.services.contracts.AddonService;
import com.example.addonisfinal.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/admin")
@RequiredArgsConstructor
public class AdminMvcController {

    private final UserService userService;
    private final AddonService addonService;

    @PreAuthorize("hasAnyAuthority('user','admin')")
    @PostMapping("/approve/{id}")
    public String approveAddon(@PathVariable int id, Principal principal) {

        User userToBeChecked = userService.getUsername(principal.getName());

        addonService.approveAddon(userToBeChecked, id);

        return "redirect:/admin/pending-addons";
    }

    @PostMapping("/notify/{id}")
    public String notifyUser(@PathVariable int id) {

        Addon currentAddon = addonService.getById(id);
        User creator = currentAddon.getCreator();
        addonService.notifyUser(creator, currentAddon);

        return "redirect:/admin/pending-addons";
    }

    @PreAuthorize("hasAuthority('admin')")
    @GetMapping("/pending-addons") //TWO
    public String getPendingAddons() {

        return "admin-panel";
    }

    @ModelAttribute("pendingAddons") //ONE
    public List<Addon> supplyPendingAddons() {

        return addonService.getAll().stream().filter(addon -> addon.getStatus().getId() == 1).collect(Collectors.toList());

    }

    @ModelAttribute("verifications") //ONE
    public List<User> pendingVerifications() {

        return userService.getAllPendingVerificationUsers();

    }

    @PreAuthorize("hasAuthority('admin')")
    @GetMapping("/pending-verifications") //TWO
    public String getPendingVerifications() {

        return "verification-form";
    }

//    @ModelAttribute("defaultAvatar")
//    public String supplyDefaultAvatarString() {
//
//        return ADDON_DEFAULT_AVATAR;
//
//    }


//    @ModelAttribute("currentUserIsAdmin")
//    public boolean currentUserIsAdmin(Principal principal) {
//
//        if (principal != null) {
//            User user = userService.getUsername(principal.getName());
//            return user.isAdmin();
//        }
//        return false;
//    }

}
