package com.example.addonisfinal.controllers.mvc;

import com.example.addonisfinal.exceptions.AuthenticationFailureException;
import com.example.addonisfinal.exceptions.DuplicateEntityException;
import com.example.addonisfinal.exceptions.EntityNotFoundException;
import com.example.addonisfinal.exceptions.UnauthorizedOperationException;
import com.example.addonisfinal.models.Addon;
import com.example.addonisfinal.models.User;
import com.example.addonisfinal.models.dtos.addon.CodeInputDto;
import com.example.addonisfinal.models.dtos.user.UserEditPasswordDto;
import com.example.addonisfinal.models.dtos.user.UserUpdateDto;
import com.example.addonisfinal.models.dtos.user.UserVerifyDto;
import com.example.addonisfinal.models.filters.UserFilter;
import com.example.addonisfinal.models.mappers.UserMapper;
import com.example.addonisfinal.services.contracts.AddonService;
import com.example.addonisfinal.services.contracts.UserService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final UserMapper userMapper;

    private final AddonService addonService;

    public UserMvcController(UserService userService, UserMapper userMapper, AddonService addonService) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.addonService = addonService;
    }

    @ModelAttribute("userFilterDto")
    public UserFilter supplyFilterSortDTO() {
        return new UserFilter();
    }

    @ModelAttribute("user")
    public UserUpdateDto supplyUpdateDTO() {
        return new UserUpdateDto();
    }

    @GetMapping("/filter/{page}")
    public String showUsers(@ModelAttribute("userFilterDto") UserFilter userFilterDto,
                            Model model,
                            @PathVariable int page,
                            Principal principal) {

        try {
            if (principal != null) {
                User currentUser = userService.getUsername(principal.getName());
                model.addAttribute("currentUser", currentUser);
            }

        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        Page<User> users = userService.getAllByMultipleFilters(
                "%" + userFilterDto.getUsername() + "%",
                page,
                6);
        model.addAttribute("users", users.getContent());
        model = supplyPageInformation(model, page, users);

        return "users";
    }

    @GetMapping("/{id}")
    public String getById(Model model, @PathVariable int id, Principal principal) {
        User currentUser = null;
        if (principal != null) {
            currentUser = userService.getUsername(principal.getName());
        }

        try {
            UserUpdateDto user = userMapper.fromUserToUserUpdateDto(userService.getById(id));
            this.prepareUserViewModels(model, currentUser, user, user, null, null, null, null);
        } catch (EntityNotFoundException e) {

            return "error";
        }

        return "user-view";
    }

    @PostMapping("/{id}")
    public String updateUser(Principal principal,
                             @PathVariable int id,
                             @Valid @ModelAttribute("user") UserUpdateDto dto,
                             Model model,
                             @RequestParam MultipartFile file) {
        User currentUser = userService.getUsername(principal.getName());
        try {
            User user = userMapper.fromUserUpdateDtoToUser(dto);
            userService.update(id, user, currentUser);
            userService.uploadAvatar(id, currentUser, file);
        } catch (EntityNotFoundException | IOException e) {
            model.addAttribute("error", e.getMessage());

            return "error";
        } catch (DuplicateEntityException e) {
            UserUpdateDto user = userMapper.fromUserToUserUpdateDto(userService.getById(id));
            List<String> errors = new ArrayList<>();
            errors.add(e.getMessage());
            this.prepareUserViewModels(model, currentUser, user, dto, null, null, errors, null);

            return "user-view";
        }

        return "redirect:/users/" + dto.getId();
    }

    @PostMapping("/{id}/change-password")
    public String changePassword(Principal principal,
                                 @PathVariable int id,
                                 @Valid @ModelAttribute("userCp") UserEditPasswordDto dto,
                                 Model model) {
        User currentUser = userService.getUsername(principal.getName());
        try {
            userService.changePassword(id, dto, currentUser);
        } catch (UnauthorizedOperationException e) {
            UserUpdateDto user = userMapper.fromUserToUserUpdateDto(userService.getById(id));
            List<String> errors = new ArrayList<>();
            errors.add(e.getMessage());
            this.prepareUserViewModels(model, currentUser, user, user, dto, null, null, errors);

            return "user-view";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());

            return "error";
        }

        return "redirect:/users/" + id;
    }

    @PostMapping("/{id}/send-verify")
    public String sendVerification(Principal principal,
                                   @PathVariable int id,
                                   @Valid @ModelAttribute("userVerify") UserVerifyDto dto,
                                   Model model) {
        User currentUser = userService.getUsername(principal.getName());
        try {
            userService.uploadVerificationPictures(id, currentUser, dto.getSelfie(), dto.getIdCard());
        } catch (EntityNotFoundException | IOException e) {
            model.addAttribute("error", e.getMessage());

            return "error";
        } catch (Exception e) {
            UserUpdateDto user = userMapper.fromUserToUserUpdateDto(userService.getById(id));
            this.prepareUserViewModels(model, currentUser, user, user, null, dto, null, null);

            return "user-view";
        }

        return "redirect:/users/" + id;
    }

    @PostMapping("/{id}/block")
    public String blockUser(Model model,
                            Principal principal,
                            @PathVariable int id,
                            @ModelAttribute("page") int page) {
        User currentUser = userService.getUsername(principal.getName());

        try {
            userService.block(id, currentUser);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());

            return "error";
        }

        return "redirect:/users/filter/" + page;
    }

    @PostMapping("/{id}/unblock")
    public String unblockUser(Model model,
                              Principal principal,
                              @PathVariable int id,
                              @ModelAttribute("page") int page) {
        User currentUser = userService.getUsername(principal.getName());

        try {
            userService.unblock(id, currentUser);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());

            return "error";
        }

        return "redirect:/users/filter/" + page;
    }

    @PostMapping("/{id}/change-permissions")
    public String changePermissions(Model model,
                                    Principal principal,
                                    @PathVariable int id,
                                    @ModelAttribute("page") int page) {
        User currentUser = userService.getUsername(principal.getName());

        try {
            userService.changePermissions(id, currentUser);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());

            return "error";
        }

        return "redirect:/users/filter/" + page;
    }

    @PostMapping("/{id}/verify")
    public String verify(Model model,
                         Principal principal,
                         @PathVariable int id,
                         @ModelAttribute("page") int page) {
        User currentUser = userService.getUsername(principal.getName());

        try {
            userService.verify(id, currentUser);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());

            return "error";
        }

        return "redirect:/users/filter/" + page;
    }

    @PostMapping("/{id}/delete")
    public String deleteUser(Model model,
                             Principal principal,
                             @PathVariable int id) {
        User currentUser = userService.getUsername(principal.getName());

        try {
            userService.delete(id, currentUser.getId());
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());

            return "error";
        }

        return "redirect:/logout";
    }

    @GetMapping("/verify-single-addon/{id}")
    public String verifySingleAddon(@PathVariable(name = "id") int id, Model model) {

        Addon addon = addonService.getById(id);
        model.addAttribute("createdAddon", addon);
        model.addAttribute("codeInput", new CodeInputDto());

        return "confirm-addon";
    }



    private Model supplyPageInformation(Model model, int page, Page<User> user) {
        if (user.hasPrevious()) {
            model.addAttribute("firstPage", false);
        }
        if (user.hasNext()) {
            model.addAttribute("lastPage", false);
        }

        model.addAttribute("page", page);

        return model;
    }

    private void prepareUserViewModels(Model model, User currentUser, UserUpdateDto user, UserUpdateDto userUpdate, UserEditPasswordDto userCp,
                                       UserVerifyDto userVerify, List<String> userUpdateErrors, List<String> userCpErrors) {
        model.addAttribute("user", user);
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("userUpdate", userUpdate);

        if (userCp == null) {
            userCp = new UserEditPasswordDto();
        }
        model.addAttribute("userCp", userCp);

        if (userVerify == null) {
            userVerify = new UserVerifyDto();
        }
        model.addAttribute("userVerify", userVerify);

        if (userUpdateErrors == null) {
            userUpdateErrors = new ArrayList<>();
        }
        model.addAttribute("userUpdateErrors", userUpdateErrors);

        if (userCpErrors == null) {
            userCpErrors = new ArrayList<>();
        }
        model.addAttribute("userCpErrors", userCpErrors);
    }
}
