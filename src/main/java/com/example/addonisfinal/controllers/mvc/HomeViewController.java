package com.example.addonisfinal.controllers.mvc;

import com.example.addonisfinal.exceptions.AlreadyRegisteredException;
import com.example.addonisfinal.exceptions.DuplicateEntityException;
import com.example.addonisfinal.exceptions.EntityNotFoundException;
import com.example.addonisfinal.exceptions.UnauthorizedOperationException;
import com.example.addonisfinal.models.Addon;
import com.example.addonisfinal.models.IDE;
import com.example.addonisfinal.models.Tag;
import com.example.addonisfinal.models.User;
import com.example.addonisfinal.models.dtos.addon.AddonDto;
import com.example.addonisfinal.models.dtos.addon.AddonDtoOut;
import com.example.addonisfinal.models.dtos.addon.AddonGridDto;
import com.example.addonisfinal.models.dtos.ContactUsDto;
import com.example.addonisfinal.models.dtos.addon.CodeInputDto;
import com.example.addonisfinal.models.dtos.user.InviteFriendDto;
import com.example.addonisfinal.models.mappers.AddonMapper;
import com.example.addonisfinal.services.contracts.AddonService;
import com.example.addonisfinal.services.contracts.IDEService;
import com.example.addonisfinal.services.contracts.TagService;
import com.example.addonisfinal.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping()
@RequiredArgsConstructor
public class HomeViewController {

    private final AddonService addonService;
    private final UserService userService;
    private final AddonMapper addonMapper;
    private final IDEService ideService;

    private final TagService tagService;

    @GetMapping
    public String showHomePage(Model model) {

        List<User> usersCount = userService.getAllUsers();
        List<AddonDtoOut> addonsCount = addonService.getAll().stream().map(addonMapper::objectToDtoOut).collect(Collectors.toList());
        List<IDE> idesCount = ideService.getAll();
        Page<Addon> addons = addonService
                .getAllByMultipleFilters("%%", "%%", "%%", "%%", "date", 1, 20);
        Page<Addon> addonsDownloaded = addonService
                .getAllByMultipleFilters("%%", "%%", "%%", "%%", "downloads_count", 1, 20);
        Page<Addon> addonsRating = addonService
                .getAllByMultipleFilters("%%", "%%", "%%", "%%", "rating", 1, 20);

        List<AddonGridDto> addonsResult = new ArrayList<>();
        for (Addon addon : addonsRating.getContent()) {
            addonsResult.add(addonMapper.objectToGridDto(addon));
        }
        List<AddonGridDto> addonsResult2 = new ArrayList<>();
        for (Addon addon : addons.getContent()) {
            addonsResult2.add(addonMapper.objectToGridDto(addon));
        }
        List<AddonGridDto> addonsResult3 = new ArrayList<>();
        for (Addon addon : addonsDownloaded.getContent()) {
            addonsResult3.add(addonMapper.objectToGridDto(addon));
        }


        model.addAttribute("addonDownload", addonsResult3);
        model.addAttribute("addonNewest", addonsResult2);
        model.addAttribute("addonRating", addonsResult);
        model.addAttribute("addonsCount", addonsCount.size());
        model.addAttribute("usersCount", usersCount.size());
        model.addAttribute("idesCount", idesCount.size());
        model.addAttribute("contactForm", new ContactUsDto());

        return "index";
    }

    @PostMapping("/contact-us")
    public String contactUsForm(@Valid @ModelAttribute("contactForm") ContactUsDto contactUsDto) {

        userService.sendContactUsEmail(contactUsDto);

        return "index :: #contact";
    }

    @PreAuthorize("hasAnyAuthority('user','admin')")
    @GetMapping("/new")
    public String showNewAddonPage(Model model) {

        model.addAttribute("addon", new AddonDto());

        return "addon-new";

    }

    @PreAuthorize("hasAnyAuthority('user','admin')")
    @GetMapping("/invite-a-friend")
    public String showInviteFriendPage(Model model) {
        model.addAttribute("inviteFriend", new InviteFriendDto());
        return "invite-a-friend";
    }

    @PreAuthorize("hasAnyAuthority('user','admin')")
    @PostMapping("/invite-a-friend")
    public String submitInvitation(@Valid @ModelAttribute("inviteFriend") InviteFriendDto inviteFriendDto, Principal principal,
                                   BindingResult bindingResult) {

        if(bindingResult.hasErrors()) {
            return "invite-a-friend";
        }

        try {
            User userToBeChecked = userService.getUsername(principal.getName());
            userService.inviteFriend(userToBeChecked, inviteFriendDto.getEmail());
        } catch (AlreadyRegisteredException e) {
            bindingResult.rejectValue("email", "email_error", e.getMessage());
            return "invite-a-friend";
        }
        return "redirect:/";
    }


    @PostMapping("/new")
    public String createAddon(@Valid @ModelAttribute("addon") AddonDto addonDto,
                              Model model,
                              Principal principal,
                              BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "addon-new";
        }

        try {
            User user = userService.getUsername(principal.getName());
            Addon addon = addonMapper.dtoToAddon(addonDto);

            addonService.saveAddon(addon, user, addonDto);
            addonMapper.setDownloadLinkToAddon(user, addon, addonDto.getFile());
            addonService.simpleUpdate(addon);
            model.addAttribute("createdAddon", addon);
            model.addAttribute("codeInput", new CodeInputDto());

        } catch (IOException | UnauthorizedOperationException e) {
            model.addAttribute("errors", e.getMessage());
            return "addon-new";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("name", "name_error", e.getMessage());
            return "addon-new";
        }
//        return "thanks-pending";


        return "confirm-addon";
    }


    @PostMapping("/confirm/{id}")
    public String confirmAddon(@PathVariable(name = "id") int id, @Valid @ModelAttribute(name = "codeInput")CodeInputDto codeInputDto,Model model, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "confirm-addon";
        }

        try {
            Addon addon = addonService.getById(id);
            model.addAttribute("createdAddon", addon);
            addonService.confirmAddon(addon, codeInputDto);
            return "thanks-pending";
        } catch (EntityNotFoundException | IllegalArgumentException e) {
            bindingResult.rejectValue("code", "code_error", e.getMessage());
            return "confirm-addon";
        }

    }

    @ModelAttribute("ides")
    public List<IDE> supplyIdes() {
        return ideService.getAll();
    }

    @ModelAttribute("tags")
    public List<Tag> supplyTags() {
        return tagService.getAll();
    }

}
