package com.example.addonisfinal.controllers.rest;

import com.example.addonisfinal.exceptions.EntityNotFoundException;
import com.example.addonisfinal.exceptions.UnauthorizedOperationException;
import com.example.addonisfinal.models.User;
import com.example.addonisfinal.services.contracts.AddonService;
import com.example.addonisfinal.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;

@RestController
@RequestMapping("/api/admin")
@RequiredArgsConstructor
public class AdminController {

    private final AddonService addonService;

    private final UserService userService;

    @PutMapping("/approve/{addonId}")
    public void approveAddon(Principal principal, @PathVariable(name = "addonId") int addonId) {

        try {
            User admin = userService.getUsername(principal.getName());
            addonService.approveAddon(admin, addonId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @PatchMapping("/{id}/block")
    public void block(Principal principal, @PathVariable int id) {
        try {
            User admin = userService.getUsername(principal.getName());
            userService.block(id, admin);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PatchMapping("/{id}/unblock")
    public void unblock(Principal principal, @PathVariable int id) {
        try {
            User admin = userService.getUsername(principal.getName());
            userService.unblock(id, admin);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PatchMapping("/{id}/change-role")
    public void changePermissions(Principal principal, @PathVariable int id) {
        try {
            User admin = userService.getUsername(principal.getName());
            userService.changePermissions(id, admin);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PatchMapping("/{id}/verify")
    public void verify(Principal principal, @PathVariable int id) {
        try {
            User admin = userService.getUsername(principal.getName());
            userService.verify(id, admin);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
