package com.example.addonisfinal.controllers.rest;

import com.example.addonisfinal.exceptions.DuplicateEntityException;
import com.example.addonisfinal.exceptions.EntityNotFoundException;
import com.example.addonisfinal.exceptions.UnauthorizedOperationException;
import com.example.addonisfinal.models.Addon;
import com.example.addonisfinal.models.Rating;
import com.example.addonisfinal.models.User;
import com.example.addonisfinal.models.dtos.addon.AddonDto;
import com.example.addonisfinal.models.dtos.addon.AddonDtoOut;
import com.example.addonisfinal.models.dtos.RatingDto;
import com.example.addonisfinal.models.filters.AddonisFilter;
import com.example.addonisfinal.models.mappers.AddonMapper;
import com.example.addonisfinal.models.mappers.RatingMapper;
import com.example.addonisfinal.services.contracts.AddonService;
import com.example.addonisfinal.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/addons")
@RequiredArgsConstructor
public class AddonController {
    private final AddonMapper addonMapper;
    private final AddonService addonService;
    private final UserService userService;

    private final RatingMapper ratingMapper;

    @GetMapping
    public List<AddonDtoOut> getAll() {
        return addonService.getAll().stream().map(addonMapper::objectToDtoOut).collect(Collectors.toList());
    }

    @GetMapping("/filter/{page}")
    public List<AddonDtoOut> multipleFilterOfAddons(@RequestParam(defaultValue = "") String addonName,
                                                    @RequestParam(defaultValue = "") String creatorName,
                                                    @RequestParam(defaultValue = "") String ideName,
                                                    @RequestParam(defaultValue = "") String orderBy,
                                                    @RequestParam(defaultValue = "") String tags,
                                                    @PathVariable int page) {

        AddonisFilter filters = new AddonisFilter(addonName, creatorName,
                ideName, tags, orderBy);

        return addonService.getAllByMultipleFilters(
                "%" + filters.getAddonName() + "%",
                "%" + filters.getCreatorUsername() + "%",
                "%" + filters.getTags() + "%",
                "%" + filters.getIdeName() + "%",
                filters.getOrderBy(),
                page,
                5).stream().map(addonMapper::objectToDtoOut).collect(Collectors.toList());

    }

    //    @PreAuthorize("hasAuthority('user')")
    @GetMapping("/{id}")
    public AddonDtoOut getById(@PathVariable int id) {
        try {
            return addonMapper.objectToDtoOut(addonService.getById(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PreAuthorize("hasAuthority('user')")
    @PostMapping("/create-addon")
    public void createAddon(Principal principal, @Valid @RequestBody AddonDto addonDto) {

        try {
            User creator = userService.getUsername(principal.getName());
            Addon addon = addonMapper.dtoToAddon(addonDto);
            addonService.saveAddon(addon, creator, addonDto);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PreAuthorize("hasAuthority('user')")
    @PutMapping("/{id}")
    public void update(Principal principal, @PathVariable int id, @RequestBody AddonDto addonDto) {
        try {
            User creator = userService.getUsername(principal.getName());
            Addon addonToUpdate = addonService.getById(id);
            addonService.updateAddon(addonToUpdate, addonDto, creator);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @PreAuthorize("hasAuthority('user')")
    @PostMapping("/{addon}/add-maintainer/{username}")
    public void promoteToMaintainer(@PathVariable(name = "addon") String addonName, @PathVariable(name = "username") String maintainerName, Principal principal) {

        try {
            User creator = userService.getUsername(principal.getName());
            addonService.promoteToMaintainer(addonName, maintainerName, creator);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PatchMapping("/approve/{addonId}")
    public void approveAddon(Principal principal, @PathVariable(name = "addonId") int addonId) {

        try {
            User admin = userService.getUsername(principal.getName());
            addonService.approveAddon(admin, addonId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @DeleteMapping("/delete/{id}")
    public void delete(Principal principal, @PathVariable int id) {
        try {
            User user = userService.getUsername(principal.getName());
            addonService.delete(user, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PreAuthorize("hasAuthority('user')")
    @PutMapping("{id}/upload-file")
    public void addFileToAddon(@PathVariable int id, MultipartFile file, Principal principal) {

        User userToBeChecked = userService.getUsername(principal.getName());

        try {
            Addon addon = addonService.getById(id);

            if (file != null) {
                addon.setFile(Base64.getEncoder().encode(file.getBytes()));
                addonMapper.setDownloadLinkToAddon(userToBeChecked, addon, file);
            }

            addonService.uploadFile(addon);

        } catch (EntityNotFoundException | IOException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PreAuthorize("hasAuthority('user')")
    @PutMapping("/{id}/rate")
    public void rate(Principal principal, @Valid @PathVariable int id, @RequestBody RatingDto ratingDTO) {
        try {
            User user = userService.getUsername(principal.getName());
            Rating rating = ratingMapper.mapRatingDtoToRating(ratingDTO);

            addonService.rateAddon(id, user, rating);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
