package com.example.addonisfinal.controllers.rest;


import com.example.addonisfinal.exceptions.AlreadyRegisteredException;
import com.example.addonisfinal.exceptions.DuplicateEntityException;
import com.example.addonisfinal.exceptions.EntityNotFoundException;
import com.example.addonisfinal.exceptions.UnauthorizedOperationException;
import com.example.addonisfinal.models.User;
import com.example.addonisfinal.models.dtos.authentication.RegisterDto;
import com.example.addonisfinal.models.dtos.user.InviteFriendDto;
import com.example.addonisfinal.models.dtos.user.RestUserDetailsDto;
import com.example.addonisfinal.models.dtos.user.UserEditPasswordDto;
import com.example.addonisfinal.models.filters.UserFilter;
import com.example.addonisfinal.models.mappers.UserMapper;
import com.example.addonisfinal.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;


    @GetMapping("/filter/{page}")
    public List<RestUserDetailsDto> multipleFilterOfUsers(@RequestParam(defaultValue = "") String username,
                                                          @PathVariable int page) {

        UserFilter filters = new UserFilter();

        return userService.getAllByMultipleFilters(
                "%" + filters.getUsername() + "%",
                page,
                5).stream().map(userMapper::userToObject).collect(Collectors.toList());

    }

    @GetMapping
    public List<RestUserDetailsDto> getAll() {
        return userService.getAllUsers().stream().map(userMapper::userToObject).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public RestUserDetailsDto getById(@PathVariable int id) {
        try {
            User user = userService.getById(id);
            return userMapper.userToObject(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/create")
    public User create(@Valid @RequestBody RegisterDto userDto) {
        try {
            User user = userMapper.userToObjectRegistration(userDto);
            userService.create(user);
            return user;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PreAuthorize("hasAuthority('user')")
    @PutMapping("/{id}")
    public void update(Principal principal,
                       @PathVariable int id,
                       @Valid @RequestBody User userDtoOut) {

        try {
            User userToBeChecked = userService.getUsername(principal.getName());
            userService.update(id, userDtoOut, userToBeChecked);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }

    }

    @PreAuthorize("hasAuthority('user')")
    @PutMapping("/{id}/upload-avatar")
    public void uploadAvatar(Principal principal, @PathVariable int id,
                             @RequestParam(value = "file", required = false) MultipartFile multipartFile) {

        try {
            User userToBeChecked = userService.getUsername(principal.getName());
            userService.uploadAvatar(id, userToBeChecked, multipartFile);
        } catch (EntityNotFoundException | IOException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @PreAuthorize("hasAuthority('user')")
    @PutMapping("/{id}/upload-selfie")
    public void uploadSelfieAndId(Principal principal, @PathVariable int id,
                                  @RequestParam(value = "selfie", required = false) MultipartFile selfie,
                                  @RequestParam(value = "idCard", required = false) MultipartFile idCard) {

        try {
            User userToBeChecked = userService.getUsername(principal.getName());
            userService.uploadVerificationPictures(id, userToBeChecked, selfie, idCard);
        } catch (EntityNotFoundException | IOException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @PreAuthorize("hasAuthority('user')")
    @PatchMapping("/{id}/update-password")
    public void updatePassword(Principal principal, @PathVariable int id,
                               @Valid @RequestBody UserEditPasswordDto userDtoOut) {

        try {
            User userToBeChecked = userService.getUsername(principal.getName());
            userService.changePassword(id, userDtoOut, userToBeChecked);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }

    }

    @PreAuthorize("hasAuthority('user')")
    @DeleteMapping("/{id}")
    public void delete(Principal principal, @PathVariable int id) {
        try {
            User userToBeChecked = userService.getUsername(principal.getName());
            userService.delete(id, userToBeChecked.getId());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @GetMapping(path = "confirm")
    public void confirm(@RequestParam("token") String token) {
        userService.confirmToken(token);
    }

    @GetMapping("/invite-a-friend")
    @PreAuthorize("hasAuthority('user')")
    public void inviteAFriend(Principal principal, @Valid @RequestBody InviteFriendDto inviteFriendDto) {

        try {
            User userToBeChecked = userService.getUsername(principal.getName());
            userService.inviteFriend(userToBeChecked, inviteFriendDto.getEmail());
        } catch (AlreadyRegisteredException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping("/{id}/resend-token")
    public void resendToken(@PathVariable int id, @Valid @RequestBody InviteFriendDto inviteFriendDto) {

        try {
            userService.resendToken(id, inviteFriendDto);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }

    }
}
