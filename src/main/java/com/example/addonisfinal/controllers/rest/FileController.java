package com.example.addonisfinal.controllers.rest;

import com.example.addonisfinal.models.Addon;
import com.example.addonisfinal.services.contracts.AddonService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Base64;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class FileController {

    private final AddonService addonService;

    @GetMapping("/download/{id}")
    public ResponseEntity<Resource> downloadAddon(@PathVariable int id) {

        Addon addon = addonService.getById(id);
        addon.setDownloadsCount(addon.getDownloadsCount() + 1);
        addonService.simpleUpdate(addon);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + addon.getFileName() + "\"")
                .body(new ByteArrayResource(Base64.getDecoder().decode(addon.getFile())));
    }

}
