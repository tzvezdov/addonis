package com.example.addonisfinal;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
@OpenAPIDefinition(info = @Info(title = "Addonis API", version = "3.0", description = "Full documentation"))
public class AddonisFinalApplication {

    public static void main(String[] args) {
        SpringApplication.run(AddonisFinalApplication.class, args);
    }

}
