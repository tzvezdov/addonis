package com.example.addonisfinal.repositories;

import com.example.addonisfinal.models.Rating;
import com.example.addonisfinal.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface RatingRepository extends JpaRepository<Rating, Integer> {

    @Query(value = "select avg(r.rating) from Ratings r where r.addon_id = ?1", nativeQuery = true)
    Optional<Double> findAverageRatingOfAddon(int addonId);

    Rating findRatingByAddon_IdAndUser(int addonId, User user);

    boolean existsByUserAndAddon_Id(User username, int id);

}
