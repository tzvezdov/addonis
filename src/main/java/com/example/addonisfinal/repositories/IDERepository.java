package com.example.addonisfinal.repositories;

import com.example.addonisfinal.models.IDE;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface IDERepository extends JpaRepository<IDE, Integer> {

    List<IDE> findAll();

    Optional<IDE> findByName(String ideName);

}
