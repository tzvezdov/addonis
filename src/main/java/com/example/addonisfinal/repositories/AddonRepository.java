package com.example.addonisfinal.repositories;

import com.example.addonisfinal.models.Addon;
import com.example.addonisfinal.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface AddonRepository extends JpaRepository<Addon, Integer> {

    List<Addon> findAll();

    Optional<Addon> findAddonById(int id);

    Optional<Addon> findAddonByName(String name);

    boolean existsByNameAndIdIsNot(String addonName, int id);

    boolean existsByOriginLink(String originLink);

    Page<Addon> findDistinctByStatus_NameAndNameLikeAndCreator_UsernameLikeAndTags_NameLikeAndIde_NameLikeAndDeletedIsFalseOrderByCreatedOnDesc(String status, String name, String creator, String tags, String ideName, Pageable pageable);

    Page<Addon> findDistinctByStatus_NameAndNameLikeAndCreator_UsernameLikeAndTags_NameLikeAndIde_NameLikeAndDeletedIsFalseOrderByName(String status, String name, String creator, String tags, String ideName, Pageable pageable);

    Page<Addon> findDistinctByStatus_NameAndNameLikeAndCreator_UsernameLikeAndTags_NameLikeAndIde_NameLikeAndDeletedIsFalseOrderByGitHubData_LastCommitDateDesc(String status, String name, String creator, String tags, String ideName, Pageable pageable);

    Page<Addon> findDistinctByStatus_NameAndNameLikeAndCreator_UsernameLikeAndTags_NameLikeAndIde_NameLikeAndDeletedIsFalseOrderByDownloadsCountDesc(String status, String name, String creator, String tags, String ideName, Pageable pageable);

    Page<Addon> findDistinctByStatus_NameAndNameLikeAndCreator_UsernameLikeAndTags_NameLikeAndIde_NameLikeAndDeletedIsFalseOrderByRatingRatingDesc(String status, String name, String creator, String tags, String ideName, Pageable pageable);

    List<Addon> findByCreatorAndDeletedIsFalse(User user);

    @Query(value = "select * from addons adds join confirmation_code cc on adds.id = cc.addon_id where cc.user_id = ?1",nativeQuery = true)
    List<Addon> findUnverifiedAddonsForUser(int id);
}
