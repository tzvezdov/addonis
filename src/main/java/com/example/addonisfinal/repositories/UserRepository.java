package com.example.addonisfinal.repositories;

import com.example.addonisfinal.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

    @Query(value = "SELECT * FROM users u JOIN additional_info ai ON ai.id = u.additional_info_id WHERE is_deleted = false", nativeQuery = true)
    List<User> findAll();

    Optional<User> findByUsername(String username);

    User getUserById(int id);

    boolean existsByUsername(String username);

    @Query(value = "SELECT * from users u join jointaddon j on u.user_id = j.user_id and j.addon_id = ?1", nativeQuery = true)
    List<User> getMaintainers(Integer addonId);

    @Modifying
    @Query(value = "update User u set u.enabled = TRUE where u.username = ?1")
    void enableUser(String username);

    @Query(value = "Select * from users u join confirmation_token ct on u.user_id = ct.user_id where token = ?1", nativeQuery = true)
    User findByToken(String token);

    boolean existsByPhoneAndIdIsNot(String phone, int id);

    boolean existsByEmailAndIdIsNot(String email, int id);

    boolean existsByEmail(String email);

    Page<User> findDistinctByEnabledIsTrueAndUsernameLikeAndDeletedIsFalse(String username, Pageable pageable);

    @Query(value = "select * from users u join additional_info ai on u.additional_info_id = ai.id where ai.id_card IS NOT NULL and ai.selfie IS NOT NULL and ai.is_verified = false and u.is_deleted = false", nativeQuery = true)
    List<User> findUsersWhereCardAndSelfiesIsNotNullAndIsVerifiedFalseAndIsDeletedFalse();

    List<User> findAllByEnabledTrueAndDeletedFalse();

}
