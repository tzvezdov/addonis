package com.example.addonisfinal.repositories;

import com.example.addonisfinal.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TagRepository extends JpaRepository<Tag, Integer> {

    List<Tag> findAll();

    Optional<Tag> findByName(String name);

}
