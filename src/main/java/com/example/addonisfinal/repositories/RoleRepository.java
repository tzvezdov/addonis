package com.example.addonisfinal.repositories;

import com.example.addonisfinal.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role,Integer> {

    Role findByName(String roleName);

}
