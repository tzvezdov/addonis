package com.example.addonisfinal.repositories;

import com.example.addonisfinal.models.Status;
import org.springframework.data.jpa.repository.JpaRepository;


public interface StatusRepository extends JpaRepository<Status, Integer> {

    Status findByName(String name);

    boolean existsByName(String name);

}
