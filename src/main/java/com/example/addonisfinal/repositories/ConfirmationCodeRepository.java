package com.example.addonisfinal.repositories;

import com.example.addonisfinal.models.Addon;
import com.example.addonisfinal.models.ConfirmationCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConfirmationCodeRepository extends JpaRepository<ConfirmationCode, Integer> {

    Optional<ConfirmationCode> findByCode(String code);

    Optional<ConfirmationCode> findByAddon_Id(int id);

}
