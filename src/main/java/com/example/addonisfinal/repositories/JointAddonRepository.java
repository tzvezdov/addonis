package com.example.addonisfinal.repositories;

import com.example.addonisfinal.models.JointAddon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface JointAddonRepository extends JpaRepository<JointAddon, Long> {

    @Query(value = "select * from jointaddon where addon_id =?1 and user_id = ?2", nativeQuery = true)
    JointAddon findJointAddonBy(int addonId, int userId);
}
