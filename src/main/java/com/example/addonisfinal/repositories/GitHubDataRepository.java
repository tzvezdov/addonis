package com.example.addonisfinal.repositories;

import com.example.addonisfinal.models.GitHubData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GitHubDataRepository extends JpaRepository<GitHubData, Long> {


}
