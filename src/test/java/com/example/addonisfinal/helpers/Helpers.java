package com.example.addonisfinal.helpers;

import com.example.addonisfinal.models.*;
import com.example.addonisfinal.models.dtos.addon.AddonDto;
import com.example.addonisfinal.models.dtos.ContactUsDto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Helpers {

    public static final String USER = "user";
    public static final String ADMIN = "admin";

    public static User createMockUser(String role) {
        User mockUser = new User();
        Set<Role> mockRole = new HashSet<>();
        mockRole.add(new Role(1, role));
        mockUser.setId(1);
        mockUser.setUsername("Tester");
        mockUser.setPassword("TestMe33!");
        mockUser.setPhone("0896991624");
        mockUser.setEnabled(true);
        mockUser.setEmail("dada@dada.com");
        mockUser.setFirstName("peshon");
        mockUser.setLastName("kurti");
        mockUser.setUserDetails(createAdditionalInfo());
        mockUser.setRoles(mockRole);
        return mockUser;
    }

    public static AdditionalInfo createAdditionalInfo() {
        AdditionalInfo infoMock = new AdditionalInfo();
        infoMock.setId(1);
        return infoMock;
    }


    public static Addon createMockAddon() {
        Addon mockAddon = new Addon();
        Set<Tag> mockTag = new HashSet<>();
        mockTag.add(new Tag(1, "java"));
        Set<Rating> ratings = new HashSet<>();
        ratings.add(new Rating());
        mockAddon.setId(14);
        mockAddon.setName("MockTest");
        mockAddon.setDescription("Test Addon Free");
        mockAddon.setIde(createIDE());
        mockAddon.setCreator(createMockUser(USER));
        mockAddon.setStatus(createStatus());
        mockAddon.setTags(mockTag);
        mockAddon.setRating(ratings);
        return mockAddon;
    }

    public static Rating createRating() {
        Rating rating = new Rating();
        rating.setId(1);
        rating.setAddon(createMockAddon());
        rating.setUser(createMockUser(ADMIN));
        rating.setRating(3);
        return rating;
    }

    public static Tag createTag() {
        Tag tag = new Tag();
        tag.setId(3);
        tag.setName("java");
        return tag;
    }

    public static GitHubData createMockData() {

        GitHubData gitHubData = new GitHubData();
        gitHubData.setLastCommitDate(new Date());
        gitHubData.setPullCount("12");
        gitHubData.setOpenIssuesCount("12");
        gitHubData.setLastCommitTitle("Title");

        return gitHubData;
    }

    public static Role createRole(){
        Role role = new Role();
        role.setId(3);
        role.setName("USER");
        return role;
    }


    public static Status createStatus() {
        Status status = new Status();
        status.setId(1);
        status.setName("ACTIVE");
        return status;
    }

    public static IDE createIDE() {
        IDE ide = new IDE();
        ide.setId(1);
        ide.setName("IntelliJ");
        return ide;
    }

    public static ContactUsDto contactUsDto(){
        ContactUsDto dto = new ContactUsDto();
        dto.setName("User");
        dto.setEmail("user@gmail.com");
        dto.setMessage("Hello");
        dto.setSubject("Mock Test");
        return dto;
    }

    public static AddonDto createDto() {
        AddonDto addonDto = new AddonDto();
        addonDto.setName("MockTest");
        addonDto.setDescription("Test Addon Free");
        addonDto.setIdeName("IntelliJ");
        addonDto.setTags("#mocktag");


        return addonDto;
    }
}
