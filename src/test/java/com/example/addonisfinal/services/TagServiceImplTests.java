package com.example.addonisfinal.services;

import com.example.addonisfinal.helpers.Helpers;
import com.example.addonisfinal.repositories.TagRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import com.example.addonisfinal.exceptions.EntityNotFoundException;
import java.util.Optional;

import static com.example.addonisfinal.helpers.Helpers.createTag;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class TagServiceImplTests {


    @Mock
    TagRepository tagRepository;

    @InjectMocks
    TagServiceImpl tagService;


    @Test
    public void getAll_Should_CallRepository() {
        Mockito.when(tagRepository.findAll()).thenReturn(null);

        tagService.getAll();

        Mockito.verify(tagRepository, Mockito.times(1)).findAll();

    }

    @Test
    public void getByNameShould_Throw_When_Tag_DoesNotExist() {
        var tag = createTag();

        Mockito.when(tagRepository.findByName(tag.getName())).thenReturn(Optional.empty());

        //Act
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> tagService.getByName(tag.getName()));

    }


    @Test
    public void getByName_Should_Call_Repository_WhenExist(){
        var tag = Helpers.createTag();

        Mockito.when(tagRepository.findByName(anyString()))
                .thenReturn(Optional.of(tag));

        tagService.getByName(tag.getName());

        Mockito.verify(tagRepository, Mockito.times(1))
                .findByName(tag.getName());

    }

    @Test
    public void addTag_Should_AddTag() {
        var mockTag = Helpers.createTag();

        tagService.addTag(mockTag);

        Mockito.verify(tagRepository, Mockito.times(1))
                .saveAndFlush(mockTag);
    }


    @Test
    public void deleteTag_Should_RemoveTag() {
        var mockTag = Helpers.createTag();

        tagService.deleteTag(mockTag);

        Mockito.verify(tagRepository, Mockito.times(1))
                .delete(mockTag);
    }

}


