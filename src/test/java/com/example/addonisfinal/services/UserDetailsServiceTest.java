package com.example.addonisfinal.services;

import com.example.addonisfinal.helpers.Helpers;
import com.example.addonisfinal.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserDetailsServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserDetailsService userDetailsService;

    @Test
    public void testLoadUserByUsername_found() {
        // Arrange
        when(userRepository.findByUsername("Tester"))
                .thenReturn(Optional.of((Helpers.createMockUser("admin"))));

        // Act
        UserDetails userDetails = userDetailsService.loadUserByUsername("Tester");

        // Assert
        assertEquals("Tester", userDetails.getUsername());
    }

    @Test
    public void testLoadUserByUsername_notFound() {
        // Arrange
        when(userRepository.findByUsername("testuser"))
                .thenReturn(Optional.empty());

        // Act and assert
        assertThrows(UsernameNotFoundException.class, () -> userDetailsService.loadUserByUsername("testuser"));
    }

}
