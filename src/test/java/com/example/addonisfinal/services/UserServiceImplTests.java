package com.example.addonisfinal.services;

import com.example.addonisfinal.exceptions.AlreadyRegisteredException;
import com.example.addonisfinal.exceptions.DuplicateEntityException;
import com.example.addonisfinal.exceptions.EntityNotFoundException;
import com.example.addonisfinal.exceptions.UnauthorizedOperationException;
import com.example.addonisfinal.helpers.Helpers;
import com.example.addonisfinal.models.AdditionalInfo;
import com.example.addonisfinal.models.Addon;
import com.example.addonisfinal.models.ConfirmationToken;
import com.example.addonisfinal.models.User;
import com.example.addonisfinal.models.dtos.user.InviteFriendDto;
import com.example.addonisfinal.models.dtos.user.RestUserDetailsDto;
import com.example.addonisfinal.models.dtos.user.UserEditPasswordDto;
import com.example.addonisfinal.repositories.ConfirmationTokenRepository;
import com.example.addonisfinal.repositories.RoleRepository;
import com.example.addonisfinal.repositories.UserRepository;
import com.example.addonisfinal.services.contracts.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;

import static com.example.addonisfinal.helpers.Helpers.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    public static final String USER = "user";
    public static final String ADMIN = "admin";

    @Mock
    BCryptPasswordEncoder passwordEncoder;
    @Mock
    UserRepository mockRepository;
    @Mock
    ConfirmationTokenRepository tokenRepository;
    @Mock
    EmailServiceImpl emailService;
    @Mock
    RoleRepository roleRepository;
    @InjectMocks
    UserServiceImpl service;
    @Mock
    ConfirmationTokenServiceImpl tokenService;

    @Test
    void findAll_Should_CallRepository() {
        // Arrange
        when(mockRepository.findAll()).thenReturn(null);

        // Act
        service.getAllUsers();

        // Assert
        verify(mockRepository, Mockito.times(1)).findAll();
    }


    @Test
    void getAllPendingVerifications_Should_CallRepository() {
        // Arrange
        when(mockRepository.findUsersWhereCardAndSelfiesIsNotNullAndIsVerifiedFalseAndIsDeletedFalse()).thenReturn(null);

        // Act
        service.getAllPendingVerificationUsers();

        // Assert
        verify(mockRepository, Mockito.times(1)).findUsersWhereCardAndSelfiesIsNotNullAndIsVerifiedFalseAndIsDeletedFalse();
    }


    @Test
    void getMaintainers_Should_CallRepository() {
        // Arrange
        Addon addon = Helpers.createMockAddon();
        when(mockRepository.getMaintainers(addon.getId())).thenReturn(null);

        // Act
        service.getMaintainers(addon.getId());

        // Assert
        verify(mockRepository, Mockito.times(1)).getMaintainers(addon.getId());
    }

    @Test
    public void getUsername_Should_ReturnUser_When_MatchByUsernameExist() {
        // Arrange
        User mockUser = Helpers.createMockUser(USER);

        when(mockRepository.findByUsername(Mockito.any()))
                .thenReturn(Optional.of(mockUser));

        // Act
        User result = service.getUsername(mockUser.getUsername());

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    void getUsername_Should_Throw_When_UserIsDeleted() {
        // Arrange
        User user = Helpers.createMockUser(ADMIN);
        user.setDeleted(true);

        // Act
        Assertions.assertThrows(
                EntityNotFoundException.class,
                () -> service.getUsername(user.getUsername()));
    }

    @Test
    public void getById_Should_ReturnUser_When_MatchByIdExist() {
        // Arrange
        User mockUser = Helpers.createMockUser(ADMIN);

        when(mockRepository.getUserById(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Act
        User result = service.getById(mockUser.getId());

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    void getById_Should_Throw_When_UserIsDeleted() {
        // Arrange
        User user = Helpers.createMockUser(ADMIN);
        user.setDeleted(true);

        // Act
        Assertions.assertThrows(
                EntityNotFoundException.class,
                () -> service.getById(user.getId()));
    }

    @Test
    public void create_Should_CallRepository_When_UserWithSameUsernameDoesNotExist() {
        // Arrange
        User mockUser = Helpers.createMockUser(USER);
        AdditionalInfo additionalInfo = Helpers.createAdditionalInfo();
        mockUser.setId(0);
        additionalInfo.setId(0);
        mockUser.setUserDetails(additionalInfo);
        mockUser.getRoles().add(roleRepository.findByName("USER"));


        // Act
        service.create(mockUser);

        // Assert
        verify(mockRepository, Mockito.times(1))
                .saveAndFlush(mockUser);
    }

    @Test
    void create_Should_Throw_When_UsernameExist() {
        // Arrange
        User user = Helpers.createMockUser(USER);
        AdditionalInfo additionalInfo = Helpers.createAdditionalInfo();

        when(mockRepository.existsByUsername(user.getUsername())).thenReturn(true);

        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(user));
    }

    @Test
    void create_Should_Throw_When_PhoneExist() {
        // Arrange
        User user = Helpers.createMockUser(USER);
        user.setId(0);

        when(mockRepository.existsByPhoneAndIdIsNot(user.getPhone(), user.getId())).thenReturn(true);

        //Act
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(user));
    }

    @Test
    void create_Should_Throw_When_EmailExist() {
        // Arrange
        User user = Helpers.createMockUser(USER);
        user.setId(0);

        when(mockRepository.existsByEmailAndIdIsNot(user.getEmail(), user.getId())).thenReturn(true);

        //Act
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(user));
    }

    @Test
    public void update_Should_CallRepository_When_UserIsProfileOwner() {
        // Arrange
        User mockUser = Helpers.createMockUser(USER);

        when(mockRepository.getUserById(mockUser.getId())).thenReturn(mockUser);

        User userToUpdate = mockRepository.getUserById(mockUser.getId());

        User userDetailsDto = new User();
        userDetailsDto.setEmail(userToUpdate.getEmail());
        userDetailsDto.setFirstName(userToUpdate.getFirstName());
        userDetailsDto.setLastName(userToUpdate.getLastName());
        userDetailsDto.setPhone(userToUpdate.getPhone());

        // Act
        service.update(mockUser.getId(), userDetailsDto, mockUser);

        // Assert
        verify(mockRepository, Mockito.times(1))
                .saveAndFlush(userToUpdate);
    }

    @Test
    public void update_Should_ThrowException_When_UserIsNotProfileOwner() {
        // Arrange
        User mockUser = Helpers.createMockUser(USER);
        User userDetailsDto = new User();
        userDetailsDto.setEmail(mockUser.getEmail());
        userDetailsDto.setFirstName(mockUser.getFirstName());
        userDetailsDto.setLastName(mockUser.getLastName());
        userDetailsDto.setPhone(mockUser.getPhone());

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> service.update(mockUser.getId(), userDetailsDto, mock(User.class)));
    }

    @Test
    void update_Should_Throw_When_PhoneExist() {
        // Arrange
        User mockUser = Helpers.createMockUser(USER);
        User userDetailsDto = new User();
        userDetailsDto.setEmail(mockUser.getEmail());
        userDetailsDto.setFirstName(mockUser.getFirstName());
        userDetailsDto.setLastName(mockUser.getLastName());
        userDetailsDto.setPhone(mockUser.getPhone());

        when(mockRepository.existsByPhoneAndIdIsNot(mockUser.getPhone(), mockUser.getId())).thenReturn(true);

        //Act
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.update(mockUser.getId(), userDetailsDto, mockUser));
    }

    @Test
    void update_Should_Throw_When_EmailExist() {
        // Arrange
        User mockUser = Helpers.createMockUser(USER);

        when(mockRepository.getUserById(mockUser.getId())).thenReturn(mockUser);

        User userToUpdate = mockRepository.getUserById(mockUser.getId());

        User userDetailsDto = new User();
        userDetailsDto.setEmail(mockUser.getEmail());
        userDetailsDto.setFirstName(mockUser.getFirstName());
        userDetailsDto.setLastName(mockUser.getLastName());
        userDetailsDto.setPhone(mockUser.getPhone());

        when(mockRepository.existsByEmailAndIdIsNot(mockUser.getEmail()
                , mockUser.getId())).thenReturn(true);

        //Act
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.update(userToUpdate.getId(), userDetailsDto, mockUser));
    }

    @Test
    public void delete_Should_UpdateUser_When_ProfileOwner() {

        // Arrange
        User mockUser = Helpers.createMockUser(USER);

        when(mockRepository.getUserById(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Act
        service.delete(mockUser.getId(), mockUser.getId());

        // Assert
        verify(mockRepository, Mockito.times(1))
                .saveAndFlush(mockUser);

    }

    @Test
    public void delete_Should_Throw_When_UserIsNotProfileOwner() {

        // Arrange
        User mockUser = Helpers.createMockUser(USER);
        User mockDifferentUser = Helpers.createMockUser(USER);
        mockDifferentUser.setId(2);

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> service.delete(mockUser.getId(), mockDifferentUser.getId()));

    }

    @Test
    public void block_Should_BlockUser_When_IsAdmin() {

        // Arrange
        User mockUser = Helpers.createMockUser(USER);
        User mockAdmin = Helpers.createMockUser(ADMIN);

        when(mockRepository.getUserById(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Act
        service.block(mockUser.getId(), mockAdmin);

        // Assert
        verify(mockRepository, Mockito.times(1))
                .saveAndFlush(mockUser);

    }

    @Test
    public void block_Should_Throw_When_IsNotAdmin() {

        // Arrange
        User mockUser = Helpers.createMockUser(USER);
        User mockAdmin = Helpers.createMockUser(USER);

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> service.block(mockUser.getId(), mockAdmin));

    }

    @Test
    public void unblock_Should_Throw_When_IsNotAdmin() {

        // Arrange
        User mockUser = Helpers.createMockUser(USER);
        User mockAdmin = Helpers.createMockUser(USER);

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> service.unblock(mockUser.getId(), mockAdmin));

    }

    @Test
    public void unblock_Should_BlockUser_When_IsAdmin() {

        // Arrange
        User mockUser = Helpers.createMockUser(USER);
        User mockAdmin = Helpers.createMockUser(ADMIN);

        when(mockRepository.getUserById(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Act
        service.unblock(mockUser.getId(), mockAdmin);

        // Assert
        verify(mockRepository, Mockito.times(1))
                .saveAndFlush(mockUser);

    }

    @Test
    public void changePermissions_Should_Promote_When_Attempted_ByAdmin() {

        //Arrange
        User mockUser = Helpers.createMockUser(USER);
        User mockAdmin = Helpers.createMockUser(ADMIN);

        when(mockRepository.getUserById(Mockito.anyInt()))
                .thenReturn(mockUser);

        //Act
        service.changePermissions(mockUser.getId(), mockAdmin);

        //Assert
        verify(mockRepository, Mockito.times(1))
                .saveAndFlush(mockUser);

    }

    @Test
    public void changePermissions_Demote_When_Attempted_ByAdmin() {

        //Arrange
        User mockUser = Helpers.createMockUser(ADMIN);
        User mockAdmin = Helpers.createMockUser(ADMIN);

        when(mockRepository.getUserById(Mockito.anyInt()))
                .thenReturn(mockUser);

        //Act
        service.changePermissions(mockUser.getId(), mockAdmin);
        verify(mockRepository, Mockito.times(1))
                .saveAndFlush(mockUser);

    }

    @Test
    public void changePermissions_Throw_When_Attempted_ByUser() {

        //Arrange
        User mockUser = Helpers.createMockUser(USER);
        User mockAdmin = Helpers.createMockUser(USER);

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> service.changePermissions(mockUser.getId(), mockAdmin));

    }

    @Test
    public void changePassword_Should_CallRepository_When_UserIsProfileOwner() {
        // Arrange
        User mockUser = Helpers.createMockUser(USER);
        BCryptPasswordEncoder newPasswordEncoder = new BCryptPasswordEncoder();
        String password = mockUser.getPassword();
        mockUser.setPassword(newPasswordEncoder.encode(mockUser.getPassword()));

        when(mockRepository.getUserById(mockUser.getId())).thenReturn(mockUser);
        UserEditPasswordDto changePasswordUser = new UserEditPasswordDto();
        changePasswordUser.setCurrentPassword(password);
        changePasswordUser.setNewPassword(newPasswordEncoder.encode(mockUser.getPassword()));
        changePasswordUser.setConfirmNewPassword(changePasswordUser.getNewPassword());

        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);

        // Act
        service.changePassword(mockUser.getId(), changePasswordUser, mockUser);

        // Assert
        verify(mockRepository, Mockito.times(1))
                .saveAndFlush(mockUser);
    }

    @Test
    public void changePassword_Should_ThrowException_When_UserIsNotProfileOwner() {
        // Arrange
        User mockUser = Helpers.createMockUser(USER);

        UserEditPasswordDto changePasswordUser = new UserEditPasswordDto();
        changePasswordUser.setCurrentPassword(mockUser.getPassword());
        changePasswordUser.setNewPassword(mockUser.getPassword());
        changePasswordUser.setConfirmNewPassword(mockUser.getPassword());

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> service.changePassword(mockUser.getId(), changePasswordUser, mock(User.class)));
    }

    @Test
    public void changePassword_Should_ThrowException_When_PasswordMissMatch() {
        // Arrange
        User mockUser = Helpers.createMockUser(USER);

        UserEditPasswordDto changePasswordUser = new UserEditPasswordDto();
        changePasswordUser.setCurrentPassword("newpassword");
        changePasswordUser.setNewPassword("newpassword");
        changePasswordUser.setConfirmNewPassword("newpassword");

        when(mockRepository.getUserById(mockUser.getId())).thenReturn(mockUser);


        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> service.changePassword(mockUser.getId(), changePasswordUser, mockUser));
    }

    @Test
    public void changePassword_Should_ThrowException_When_ConfirmDifferentFromNew() {
        // Arrange
        User mockUser = Helpers.createMockUser(USER);
        BCryptPasswordEncoder newPasswordEncoder = new BCryptPasswordEncoder();
        String password = mockUser.getPassword();
        mockUser.setPassword(newPasswordEncoder.encode(mockUser.getPassword()));

        UserEditPasswordDto changePasswordUser = new UserEditPasswordDto();
        changePasswordUser.setCurrentPassword(password);
        changePasswordUser.setNewPassword("password");
        changePasswordUser.setConfirmNewPassword("newpassword");

        when(mockRepository.getUserById(mockUser.getId())).thenReturn(mockUser);
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);


        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> service.changePassword(mockUser.getId(), changePasswordUser, mockUser));
    }

    @Test
    public void verify_Should_VerifyUser_When_IsAdmin() {

        // Arrange
        User mockUser = Helpers.createMockUser(USER);
        User mockAdmin = Helpers.createMockUser(ADMIN);

        when(mockRepository.getUserById(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Act
        service.verify(mockUser.getId(), mockAdmin);

        // Assert
        verify(mockRepository, Mockito.times(1))
                .saveAndFlush(mockUser);

    }

    @Test
    public void verify_Should_Throw_When_IsNotAdmin() {

        // Arrange
        User mockUser = Helpers.createMockUser(USER);
        User mockAdmin = Helpers.createMockUser(USER);

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> service.verify(mockUser.getId(), mockAdmin));

    }

    @Test
    public void inviteFriend_Should_SendEmail() {

        // Arrange
        User mockUser = Helpers.createMockUser(USER);
        String invitation = "http://localhost:8080/api/users/create";

        // Act
        service.inviteFriend(mockUser, invitation);
    }

    @Test
    public void confirmToken_Should_CallRepository_When_TokenIsVerified() {
        // Arrange
        User mockUser = Helpers.createMockUser(USER);
        ConfirmationToken token = new ConfirmationToken();
        token.setToken("something");
        token.setUser(mockUser);
        token.setIssuedAt(LocalDateTime.now());
        token.setExpiresAt(LocalDateTime.now().plusMinutes(15));

        when(tokenService.getToken(anyString())).thenReturn(Optional.of(token));

        // Act
        when(mockRepository.findByToken(token.getToken())).thenReturn(mockUser);

        // Assert
        service.confirmToken(token.getToken());

    }

    @Test
    public void resendToken_Should_Throw_When_Unauthorized(){
        User user = createMockUser(USER);
        InviteFriendDto user2 = new InviteFriendDto();
        user2.setEmail("lolo");

        when(mockRepository.getUserById(anyInt())).thenReturn(user);


        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> service.resendToken(anyInt(),user2));
    }


    @Test
    public void resendToken_Should_Throw_When_Duplicate(){
        User user = createMockUser(USER);
        user.setEmail("lolo");
        InviteFriendDto user2 = new InviteFriendDto();
        user2.setEmail("lolo");

        when(mockRepository.getUserById(anyInt())).thenReturn(user);


        Assertions.assertThrows(
                DuplicateEntityException.class,
                () -> service.resendToken(anyInt(),user2));

    }

    @Test
    public void resendToken_Should_Send_Email(){
        User user = createMockUser(USER);
        user.setEnabled(false);
        user.setEmail("lolo");
        InviteFriendDto user2 = new InviteFriendDto();
        user2.setEmail("lolo");

        ConfirmationToken token = new ConfirmationToken();
        token.setUser(user);
        token.setIssuedAt(LocalDateTime.now());
        token.setExpiresAt(LocalDateTime.now().plusMinutes(15));
        token.setConfirmedAt(LocalDateTime.now());

        when(mockRepository.getUserById(anyInt())).thenReturn(user);

        service.resendToken(user.getId(),user2);

        tokenService.saveConfirmationToken(token);

        emailService.send(anyString(),anyString());
    }

    @Test
    public void inviteAFriend_Should_Throw_When_EmailExists() {

        //Arrange
        User user = createMockUser(USER);
        user.setEmail("testing@abv.bg");

        //Act
        when(mockRepository.existsByEmail(anyString())).thenReturn(true);


        // Assert
        Assertions.assertThrows(
                AlreadyRegisteredException.class,
                () -> service.inviteFriend(user, anyString()));
    }

    @Test
    public void uploadAvatar_Should_Throw_When_User_Is_NotOwner() {
        User user = createMockUser(USER);
        user.setId(30);

        when(mockRepository.getUserById(anyInt())).thenReturn(user);

        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> service.uploadAvatar(anyInt(),createMockUser(USER),new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes())));
    }

    @Test
    public void uploadAvatar_Should_Call_Repository() throws IOException {
        User user = createMockUser(USER);

        when(mockRepository.getUserById(anyInt())).thenReturn(user);

        service.uploadAvatar(user.getId(), user, new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes()));

        verify(mockRepository, Mockito.times(1))
                .saveAndFlush(user);
    }


    @Test
    public void uploadVerificationPictures_Should_Throw_When_User_Is_NotOwner() {
        User user = createMockUser(USER);
        user.setId(30);
        MockMultipartFile firstFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());
        MockMultipartFile secondFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());


        when(mockRepository.getUserById(anyInt())).thenReturn(user);

        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> service.uploadVerificationPictures(anyInt(),createMockUser(USER),firstFile,secondFile));
    }


    @Test
    public void uploadVerificationPictures_Should_Call_Repository() throws IOException {
        User user = createMockUser(USER);
        MockMultipartFile firstFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());
        MockMultipartFile secondFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());


        when(mockRepository.getUserById(anyInt())).thenReturn(user);

        service.uploadVerificationPictures(user.getId(), user, firstFile,secondFile);

        verify(mockRepository, Mockito.times(1))
                .saveAndFlush(user);
    }


    @Test
    public void confirmToken_Should_Throw_When_EmailIsConfirmed() {
        // Arrange
        User mockUser = Helpers.createMockUser(USER);
        ConfirmationToken token = new ConfirmationToken();
        token.setToken("something");
        token.setUser(mockUser);
        token.setIssuedAt(LocalDateTime.now());
        token.setExpiresAt(LocalDateTime.now().plusMinutes(15));
        token.setConfirmedAt(LocalDateTime.now());

        //Act
        when(tokenService.getToken(anyString())).thenReturn(Optional.of(token));

        // Assert
        Assertions.assertThrows(
                IllegalStateException.class,
                () -> service.confirmToken(anyString()));

    }

    @Test
    public void confirmToken_Should_Throw_When_TokenExpired() {
        // Arrange
        User mockUser = Helpers.createMockUser(USER);
        ConfirmationToken token = new ConfirmationToken();
        token.setToken("something");
        token.setUser(mockUser);
        token.setIssuedAt(LocalDateTime.now());
        token.setExpiresAt(LocalDateTime.now().minusHours(2));

        //Act
        when(tokenService.getToken(anyString())).thenReturn(Optional.of(token));

        // Assert
        Assertions.assertThrows(
                IllegalStateException.class,
                () -> service.confirmToken(anyString()));

    }


    @Test
    public void send_ContactEmail_Should_Call_SendEmail() {
        var contactForm = contactUsDto();

        service.sendContactUsEmail(contactForm);

    }

}
