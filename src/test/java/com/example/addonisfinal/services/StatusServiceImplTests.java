package com.example.addonisfinal.services;

import com.example.addonisfinal.helpers.Helpers;
import com.example.addonisfinal.models.Status;
import com.example.addonisfinal.repositories.StatusRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import com.example.addonisfinal.exceptions.EntityNotFoundException;

import static com.example.addonisfinal.helpers.Helpers.createStatus;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class StatusServiceImplTests {


    @Mock
    StatusRepository statusRepository;

    @InjectMocks
    StatusServiceImpl statusService;



    @Test
    public void getByStatus_Should_Throw_WhenStatusNotExist() {

        Mockito.when(statusRepository.existsByName(anyString())).thenReturn(false);

        //Act
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> statusService.getStatusByName(anyString()));

    }


    @Test
    public void getByStatus_Should_Call_Repository_WhenExist(){
        var status = Helpers.createStatus();

        Mockito.when(statusRepository.existsByName(Mockito.anyString()))
                .thenReturn(true);
        //Act
        statusService.getStatusByName(status.getName());
        //Assert
        Mockito.verify(statusRepository, Mockito.times(1))
                .findByName(status.getName());

    }

    @Test
    public void createStatus_Should_Call_Repository() {
        Status status = createStatus();

        statusService.createStatus(status);

        Mockito.verify(statusRepository, Mockito.times(1))
                .saveAndFlush(status);
    }

}
