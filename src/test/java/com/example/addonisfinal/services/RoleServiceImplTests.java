package com.example.addonisfinal.services;

import com.example.addonisfinal.repositories.RoleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import com.example.addonisfinal.exceptions.EntityNotFoundException;

import static com.example.addonisfinal.helpers.Helpers.createRole;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class RoleServiceImplTests {

    @Mock
    RoleRepository roleRepository;

    @InjectMocks
    RoleServiceImpl roleService;



    @Test
    public void getByNameShould_Throw_When_Role_DoesNotExist() {
        var role = createRole();

        Mockito.when(roleRepository.findByName(role.getName())).thenReturn(null);

        //Act
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> roleService.getByName(role.getName()));

    }


    @Test
    public void getByName_Should_Call_Repository_WhenExist() {
        var role = createRole();

        Mockito.when(roleRepository.findByName(anyString()))
                .thenReturn(role);

        roleService.getByName(role.getName());

        Mockito.verify(roleRepository, Mockito.times(1))
                .findByName(role.getName());

    }
}
