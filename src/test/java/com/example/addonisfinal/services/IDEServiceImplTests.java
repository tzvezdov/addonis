package com.example.addonisfinal.services;

import com.example.addonisfinal.repositories.IDERepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import com.example.addonisfinal.exceptions.EntityNotFoundException;
import java.util.Optional;

import static com.example.addonisfinal.helpers.Helpers.createIDE;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class IDEServiceImplTests {

    @Mock
    IDERepository ideRepository;

    @InjectMocks
    IDEServiceImpl ideService;


    @Test
    public void getAll_Should_CallRepository() {
        Mockito.when(ideRepository.findAll()).thenReturn(null);

        ideService.getAll();

        Mockito.verify(ideRepository, Mockito.times(1)).findAll();

    }

    @Test
    public void getByNameShould_Throw_When_IDE_DoesNotExist() {
        var ide = createIDE();

        Mockito.when(ideRepository.findByName(ide.getName())).thenReturn(Optional.empty());

        //Act
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> ideService.getByName(ide.getName()));

    }


    @Test
    public void getByName_Should_Call_Repository_WhenExist() {
        var ide = createIDE();

        Mockito.when(ideRepository.findByName(anyString()))
                .thenReturn(Optional.of(ide));

        ideService.getByName(ide.getName());

        Mockito.verify(ideRepository, Mockito.times(1))
                .findByName(ide.getName());

    }

    @Test
    public void createIDE_Should_Call_Repository() {
        var ide = createIDE();

        ideService.createIDE(ide);

        Mockito.verify(ideRepository, Mockito.times(1))
                .saveAndFlush(ide);
    }
}
