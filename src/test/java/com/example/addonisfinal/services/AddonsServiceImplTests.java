package com.example.addonisfinal.services;

import com.example.addonisfinal.exceptions.DuplicateEntityException;
import com.example.addonisfinal.exceptions.EntityNotFoundException;
import com.example.addonisfinal.exceptions.UnauthorizedOperationException;
import com.example.addonisfinal.helpers.TagHelper;
import com.example.addonisfinal.models.*;
import com.example.addonisfinal.models.dtos.addon.AddonDto;
import com.example.addonisfinal.repositories.AddonRepository;
import com.example.addonisfinal.repositories.JointAddonRepository;
import com.example.addonisfinal.repositories.RatingRepository;
import com.example.addonisfinal.services.contracts.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;

import static com.example.addonisfinal.helpers.Helpers.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class AddonsServiceImplTests {

    @Mock
    AddonRepository mockRepository;

    @Mock
    TagHelper tagHelper;
    @Mock
    RoleService roleService;
    @Mock
    StatusService statusService;
    @Mock
    UserService userService;

    @Mock
    RatingService ratingService;
    @Mock
    JointAddonRepository jointAddonRepository;

    @Mock
    EmailSender emailSender;
    @Mock
    IDEService ideService;

    @Mock
    GitHubDataService gitHubDataService;

    @Mock
    RatingRepository ratingRepository;

    @InjectMocks
    AddonServiceImpl service;


    @Test
    public void getAll_Should_CallRepository() {
        when(mockRepository.findAll())
                .thenReturn(null);
        service.getAll();
        Mockito.verify(mockRepository, Mockito.times(1))
                .findAll();

    }

    @Test
    public void getByUser_Should_ReturnList_Of_UserAddons_When_UsersExists() {
        when(mockRepository.findByCreatorAndDeletedIsFalse(createMockUser(USER)))
                .thenReturn(null);
        service.getByUser(createMockUser(USER));
        Mockito.verify(mockRepository, Mockito.times(1))
                .findByCreatorAndDeletedIsFalse(createMockUser(USER));
    }

    @Test
    public void getById_Should_ReturnAddon_When_AddonExists() {
        Addon addon = createMockAddon();

        when(mockRepository.findAddonById(Mockito.anyInt()))
                .thenReturn(Optional.of(addon));

        Addon result = service.getById(1);

        Assertions.assertSame(addon, result);

    }

    @Test
    public void getByIdShould_Throw_WhenAddonNotExist() {
        Addon mockAddon = createMockAddon();

        when(mockRepository.findAddonById(mockAddon.getId())).thenReturn(Optional.empty());

        //Act
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.getById(mockAddon.getId()));

    }

    @Test
    public void findByName_Should_Throw_WhenAddonNotExist() {

        when(mockRepository.findAddonByName(anyString())).thenReturn(Optional.empty());

        //Act
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.findByName(anyString()));

    }


    //Questionable ?
    @Test
    public void saveAddon_Should_Call_Repository() throws IOException {

        Addon mockAddon = createMockAddon();
        User creator = createMockUser(USER);
        AddonDto dto = createDto();
        MockMultipartFile firstFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());


        mockAddon.setCreator(creator);
        mockAddon.setCreatedOn(LocalDateTime.now());
        dto.setImage(firstFile);
        dto.setFile(firstFile);
        dto.setOriginLink("https://github.com/codecadwallader/codemaid");
        GitHubData gitHubData = createMockData();
        mockAddon.setGitHubData(gitHubData);
        when(gitHubDataService.validateLink(anyString())).thenReturn(true);
        when(ideService.getByName(anyString())).thenReturn(mockAddon.getIde());
        when(statusService.getStatusByName(anyString())).thenReturn(mockAddon.getStatus());

        service.saveAddon(mockAddon, creator, dto);

        Mockito.verify(mockRepository, Mockito.times(1))
                .saveAndFlush(mockAddon);
    }

    @Test
    public void saveAddon_Should_Throw_WhenNameExists() {
        Addon mockAddon = createMockAddon();

        when(mockRepository.existsByName(mockAddon.getName())).thenReturn(true);

        Assertions.assertThrows(DuplicateEntityException.class, () -> service.saveAddon(mockAddon, createMockUser(USER), createDto()));

    }

    @Test
    public void saveAddon_Should_Throw_When_Unauthorized() {

        Addon mockAddon = createMockAddon();
        User creator = mockAddon.getCreator();
        creator.getUserDetails().setBlocked(true);

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.saveAddon(mockAddon, creator, createDto()));

    }

    @Test
    public void findByName_Should_CallRepository() {

        Addon mockAddon = createMockAddon();

        when(mockRepository.findAddonByName((anyString()))).thenReturn(Optional.of(mockAddon));

        Addon existing = service.findByName(anyString());

        Assertions.assertSame(mockAddon, existing);
    }


    @Test
    public void update_Should_Call_Repository() throws IOException {
        Addon mockAddon = createMockAddon();
        AddonDto mockAddonDto = createDto();
        MockMultipartFile firstFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());

        mockAddonDto.setOriginLink("https://github.com/codecadwallader/codemaid");
        GitHubData gitHubData = createMockData();
        mockAddon.setGitHubData(gitHubData);
        mockAddonDto.setImage(firstFile);
        when(gitHubDataService.validateLink(anyString())).thenReturn(true);


        service.updateAddon(mockAddon, mockAddonDto, createMockUser(USER));

        Mockito.verify(mockRepository, Mockito.times(1)).saveAndFlush(mockAddon);

    }

    @Test
    public void update_Should_Throw_When_NameExist() {
        Addon mockAddon = createMockAddon();

        when(mockRepository.existsByName(mockAddon.getName())).thenReturn(true);

        Assertions.assertThrows(DuplicateEntityException.class, () -> service.updateAddon(mockAddon, createDto(), createMockUser(USER)));
    }

    @Test
    public void update_Should_Throw_When_Unauthorized() {
        Addon mockAddon = createMockAddon();
        User mockUser = createMockUser(USER);
        mockUser.setId(87);

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.updateAddon(mockAddon, createDto(), mockUser));
    }


    @Test
    public void delete_Should_Call_Repository() {
        Addon mockAddon = createMockAddon();

        when(mockRepository.findAddonById(anyInt())).thenReturn(Optional.of(mockAddon));

        service.delete(mockAddon.getCreator(), anyInt());

        Mockito.verify(mockRepository, Mockito.times(1)).saveAndFlush(mockAddon);
    }

    @Test
    public void delete_Should_Throw_When_Unauthorized() {

        Addon mockAddon = createMockAddon();
        User mockUser = createMockUser(USER);
        mockUser.setUsername("Despair");
        mockUser.setId(4);

        when(mockRepository.findAddonById(anyInt())).thenReturn(Optional.of(mockAddon));

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.delete(mockUser, anyInt()));

    }

    @Test
    public void delete_Should_Throw_WhenAddonNotExist() {
        Addon mockAddon = createMockAddon();

        when(mockRepository.findAddonById(anyInt())).thenReturn(Optional.empty());

        //Act
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.delete(createMockUser(USER), anyInt()));

    }

    @Test
    public void approveAddon_Should_Call_Repository() {

        Addon mockAddon = createMockAddon();
        User mockUser = createMockUser(USER);
        Role adminRole = new Role(1, ADMIN);
        mockUser.getRoles().add(adminRole);

        when(mockRepository.findAddonById(anyInt())).thenReturn(Optional.of(mockAddon));

        service.approveAddon(mockUser, mockAddon.getId());

        Mockito.verify(mockRepository, Mockito.times(1)).save(mockAddon);
    }

    @Test
    public void approveAddon_Should_Throw_When_NoSuchAddon() {

        Addon mockAddon = createMockAddon();

        when(mockRepository.findAddonById(anyInt())).thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.approveAddon(mockAddon.getCreator(), anyInt()));

    }

    @Test
    public void approveAddon_Should_Throw_When_Unauthorized() {

        Addon mockAddon = createMockAddon();

        when(mockRepository.findAddonById(anyInt())).thenReturn(Optional.of(mockAddon));

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.approveAddon(mockAddon.getCreator(), anyInt()));
    }

    @Test
    public void approveAddon_Should_Throw_When_Duplicated() {
        Addon mockAddon = createMockAddon();
        Status status = createStatus();
        mockAddon.setStatus(status);


        when(mockRepository.findAddonById(anyInt())).thenReturn(Optional.of(mockAddon));
        when(statusService.getStatusByName(anyString())).thenReturn(status);


        Assertions.assertThrows(DuplicateEntityException.class, ()
                -> service.approveAddon(mockAddon.getCreator(), mockAddon.getId()));
    }

    @Test
    public void promoteToMaintainer_Should_Call_Repository() {

        Addon mockAddon = createMockAddon();
        User toBeMaintainer = createMockUser(USER);
        toBeMaintainer.setId(87);
        JointAddon jointAddon = new JointAddon();
        jointAddon.setUser(toBeMaintainer);
        jointAddon.setAddon(mockAddon);

        when(mockRepository.findAddonByName(anyString())).thenReturn(Optional.of(mockAddon));

        service.promoteToMaintainer(mockAddon.getName(), toBeMaintainer.getUsername(), mockAddon.getCreator());
        jointAddonRepository.save(jointAddon);

        Mockito.verify(jointAddonRepository, Mockito.times(1)).save(jointAddon);
    }

    @Test
    public void promoteToMaintainer_Should_Throw_When_Unauthorized() {
        Addon mockAddon = createMockAddon();
        User user = createMockUser(USER);
        user.setId(40);
        mockAddon.setCreator(user);


        when(mockRepository.findAddonByName(anyString())).thenReturn(Optional.of(mockAddon));

        Assertions.assertThrows(UnauthorizedOperationException.class, ()
                -> service.promoteToMaintainer(mockAddon.getName(), anyString(), createMockUser(USER)));
    }

    @Test
    public void promoteToMaintainer_Should_Throw_WhenAddonNotExist() {
        Addon mockAddon = createMockAddon();

        when(mockRepository.findAddonByName(anyString())).thenReturn(Optional.empty());

        //Act
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.promoteToMaintainer(mockAddon.getName(), anyString(), createMockUser(USER)));

    }

    @Test
    public void gitHub_Should_Throw_WhenUrl_DoesNot_Exist() {
        String url = "Test";

        gitHubDataService.validateLink(url);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> service.getGitHubData(anyString(), new GitHubData()));
    }

    @Test
    public void rateAddon_Should_ChangeRating_When_Exists() {
        var addon = createMockAddon();
        var rating = createRating();
        rating.setRating(anyInt());
        ratingRepository.saveAndFlush(rating);
        addon.getRating().add(rating);

        when(mockRepository.findAddonById(anyInt())).thenReturn(Optional.of(addon));

        service.rateAddon(anyInt(), rating.getUser(), rating);

    }

    @Test
    public void rateAddon_Should_CreateRating_When_DoesNotExists() {
        var addon = createMockAddon();
        var rating2 = createRating();
        rating2.setRating(4);
        rating2.setId(55);
        rating2.setUser(createMockUser(USER));
        ratingRepository.saveAndFlush(rating2);

        when(mockRepository.findAddonById(anyInt())).thenReturn(Optional.of(addon));
        service.rateAddon(anyInt(), rating2.getUser(), rating2);

        Mockito.verify(ratingRepository, Mockito.times(1)).saveAndFlush(rating2);


    }

    @Test
    public void notifyUser_Should_Send_Email() {
        Addon mockAddon = createMockAddon();
        mockAddon.setStatus(createStatus());

        when(statusService.getStatusByName(anyString())).thenReturn(mockAddon.getStatus());

        emailSender.send(anyString(), anyString());
        service.notifyUser(createMockUser(USER), mockAddon);

        Mockito.verify(mockRepository, Mockito.times(1)).saveAndFlush(mockAddon);
    }

    @Test
    public void simpleUpdate_Should_Call_Repository() {
        Addon mockAddon = createMockAddon();

        service.simpleUpdate(mockAddon);

        Mockito.verify(mockRepository, Mockito.times(1))
                .saveAndFlush(mockAddon);
    }

    @Test
    public void uploadFile_Should_Call_Repository() {
        Addon mockAddon = createMockAddon();

        service.uploadFile(mockAddon);

        Mockito.verify(mockRepository, Mockito.times(1))
                .saveAndFlush(mockAddon);
    }

}
