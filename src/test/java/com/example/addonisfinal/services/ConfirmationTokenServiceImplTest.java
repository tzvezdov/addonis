package com.example.addonisfinal.services;

import com.example.addonisfinal.models.ConfirmationToken;
import com.example.addonisfinal.repositories.ConfirmationTokenRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ConfirmationTokenServiceImplTest {

    @InjectMocks
    ConfirmationTokenServiceImpl service;

    @Mock
    ConfirmationTokenRepository repository;

    @Mock
    ConfirmationToken confirmationToken;

    @Test
    void saveConfirmationToken_Should_CallRepository() {

        //Arrange
        //Act
        service.saveConfirmationToken(confirmationToken);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).save(confirmationToken);
    }

    @Test
    void getToken_Should_CallRepository() {

        //Arrange
        confirmationToken.setToken("something");

        //Act
        Mockito.when(repository.findByToken(confirmationToken.getToken())).thenReturn(Optional.of(confirmationToken));
        service.getToken(confirmationToken.getToken());

        //Assert
        Mockito.verify(repository, Mockito.times(1)).findByToken(confirmationToken.getToken());
    }

    @Test
    void setConfirmedAt_Should_CallRepository() {

        //Arrange
        confirmationToken.setToken("something");

        //Act
        Mockito.when(repository.findByToken(confirmationToken.getToken())).thenReturn(Optional.of(confirmationToken));
        service.setConfirmedAt(confirmationToken.getToken());

        //Assert
        Mockito.verify(repository, Mockito.times(1)).saveAndFlush(confirmationToken);


//        Optional<ConfirmationToken> confirmationToken = tokenRepository.findByToken(token);
//        confirmationToken.ifPresent(value -> value.setConfirmedAt(LocalDateTime.now()));
//        tokenRepository.saveAndFlush(confirmationToken.get());
    }

}
