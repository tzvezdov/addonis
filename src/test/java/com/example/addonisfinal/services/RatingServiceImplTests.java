package com.example.addonisfinal.services;

import com.example.addonisfinal.exceptions.EntityNotFoundException;
import com.example.addonisfinal.exceptions.UnauthorizedOperationException;
import com.example.addonisfinal.helpers.Helpers;
import com.example.addonisfinal.models.Addon;
import com.example.addonisfinal.models.Rating;
import com.example.addonisfinal.models.dtos.RatingDto;
import com.example.addonisfinal.repositories.AddonRepository;
import com.example.addonisfinal.repositories.RatingRepository;
import com.example.addonisfinal.services.contracts.AddonService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.*;

@ExtendWith(MockitoExtension.class)
public class RatingServiceImplTests {


    @Mock
    AddonRepository addonRepository;

    @Mock
    AddonService addonService;

    @Mock
    RatingRepository ratingRepository;

    @InjectMocks
    RatingServiceImpl service;


    @Test
    public void getAvgRating_OfAddon_Should_Throw_When_No_Such_Rating(){
        Rating rating = Helpers.createRating();
        Addon addon = Helpers.createMockAddon();
        rating.setRating(6);
        addon.getRating().add(rating);


        Assertions.assertThrows(
                EntityNotFoundException.class,
                () -> service.getAvgRatingOfAddon(addon.getId()));
    }


    @Test
    public void getAvgRating_Should_Call_Repository_WhenExist(){
        Addon addon =Helpers.createMockAddon();

        Mockito.when(ratingRepository.findAverageRatingOfAddon(anyInt()))
                .thenReturn(Optional.of(anyDouble()));

        service.getAvgRatingOfAddon(addon.getId());

        Mockito.verify(ratingRepository, Mockito.times(1))
                .findAverageRatingOfAddon(addon.getId());

    }

}
