package com.example.addonisfinal.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import javax.mail.internet.MimeMessage;

@ExtendWith(MockitoExtension.class)
public class EmailServiceImplTests {

    @InjectMocks
    EmailServiceImpl service;

    @Mock
    JavaMailSenderImpl mailSender;

    @Mock
    MimeMessage mimeMessage;

    @Test
    void send_ShouldSend_MessageToUser() {

        Mockito.when(mailSender.createMimeMessage()).thenReturn(mimeMessage);

        service.send("to@mail.com", "email@email.com");

        Mockito.verify(mailSender, Mockito.times(1)).send(mimeMessage);

    }

    @Test
    public void sendContactForm_Should_Send_MessageToUser(){
        Mockito.when(mailSender.createMimeMessage()).thenReturn(mimeMessage);

        service.sendContactForm("from@mail.com", "email@email.com");

        Mockito.verify(mailSender, Mockito.times(1)).send(mimeMessage);
    }
}
