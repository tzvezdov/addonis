
# Addonis

## Description

**Addonis** is a web application that gives users the opportunity to manage add-ons. The Application is with three types of users:

- anonymous;

- regular;

- administrator.

  

Anonymous users are able to filter addons by name or IDE and sort by the name, number of downloads, upload date, last commit date. They can also download a plugin, directly from the landing page.

  

Registered users have private area in the web application accessible after successful login, where they could see all extensions that are owned by the currently logged user. Additionally, the registered users are able to delete, update, create their own addons and rate all addons. Once addon is created the it is "pending" state until the administrator approves it. The extension is visible in the landing page only if it is approved.

  

System administrators can administer all major information objects in the system. On top of the regular user capabilities, the administrators have the following capabilities:

- to approve new addons;

- delete or edit all addons;

- disable users accounts.

  
  

## Technologies

- Spring MVC

- Spring Security

- Hibernate

- Spring Data JPA

- Thymeleaf

- HTML

- CSS

- JUnit

  

## Database

![picture](/images/addonis_db.png)

  

## WebApp Screenshots

  

- Landing Page (URL **"/"**)- landing page with three categories - newest addons, most popular, most rated.

![picture](images/home_page_addons.png)

  

- All Addons Page (URL **"addons/filter/1"**)- page with all addons with status "approved" and their filter and sort options.

![picture](images/all_addons.png)



- Create new Addon (URL **"/addons/new"**)- form for create new addons.

![picture](images/create-addon.png)

  

- Update Addon (URL **"addons/{id}/update"**)- form for update details (without origin link and executable) of addon.

![picture](images/update-addon.png)

  - Users admin view (URL **"/users"**) - how admins see all existing users. 

![picture](images/admin-users-view.png)

  - User profile  Unverified (URL **"/users/{Id}"**)- page with detail information for the user.
![picture](images/user-view-unverified.png)


- User profile Verified (URL **"/users/{Id}"**)
![picture](images/user-verified.png)

   
Swagger documentation:  [http://localhost:8080/swagger-ui/index.html](http://localhost:8080/swagger-ui/index.html "http://localhost:8080/swagger-ui/index.html")

## Authors
-  [@tzvezdov](https://gitlab.com/tzvezdov)
-  [@kkrushkov](https://gitlab.com/k.krushkov1)
-  [@Tihomir Angelov](https://gitlab.com/NordilJ)